-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for deliverit
DROP DATABASE IF EXISTS `deliverit`;
CREATE DATABASE IF NOT EXISTS `deliverit` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `deliverit`;

-- Dumping structure for table deliverit.addresses
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(30) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`primary_key`),
  UNIQUE KEY `addresses_address_uindex` (`address`),
  KEY `addresses_cities_primary_key_fk` (`city_id`),
  CONSTRAINT `addresses_cities_primary_key_fk` FOREIGN KEY (`city_id`) REFERENCES `cities` (`primary_key`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.addresses: ~11 rows (approximately)
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` (`primary_key`, `address`, `city_id`) VALUES
	(1, 'Glavnata 2', 1),
	(2, 'Morska 4', 2),
	(3, 'st.Rogers', 3),
	(4, 'st.Clay', 4),
	(5, 'Treti Mart', 5),
	(6, 'Louis Andrews', 6),
	(7, '5th Avenue', 9),
	(8, 'Sunset Blvd.', 10),
	(9, 'Hans Zimmer 10', 7),
	(10, 'Thomas Straße', 8),
	(11, 'Showa dori', 11);
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;

-- Dumping structure for table deliverit.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(30) NOT NULL,
  PRIMARY KEY (`primary_key`),
  UNIQUE KEY `categories_category_uindex` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`primary_key`, `category`) VALUES
	(2, 'Clothes'),
	(1, 'Electronics');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table deliverit.cities
DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`primary_key`),
  UNIQUE KEY `cities_name_uindex` (`name`),
  KEY `cities_countries_primary_key_fk` (`country_id`),
  CONSTRAINT `cities_countries_primary_key_fk` FOREIGN KEY (`country_id`) REFERENCES `countries` (`primary_key`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.cities: ~11 rows (approximately)
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` (`primary_key`, `name`, `country_id`) VALUES
	(1, 'Sofia', 1),
	(2, 'Varna', 1),
	(3, 'London', 2),
	(4, 'Birmingham', 2),
	(5, 'Burgas', 1),
	(6, 'Glasgow', 2),
	(7, 'Berlin', 4),
	(8, 'Hamburg', 4),
	(9, 'New York', 3),
	(10, 'Los Angeles', 3),
	(11, 'Tokyo', 5);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;

-- Dumping structure for table deliverit.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`primary_key`),
  UNIQUE KEY `countries_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.countries: ~5 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`primary_key`, `name`) VALUES
	(1, 'Bulgaria'),
	(4, 'Germany'),
	(5, 'Japan'),
	(2, 'UK'),
	(3, 'USA');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table deliverit.parcels
DROP TABLE IF EXISTS `parcels`;
CREATE TABLE IF NOT EXISTS `parcels` (
  `parcel_id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_id` int(11) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `shipment_id` int(11) DEFAULT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`parcel_id`),
  KEY `parcels_categories_primary_key_fk` (`category_id`),
  KEY `parcels_shipments_primary_key_fk` (`shipment_id`),
  KEY `parcels_warehouses_primary_key_fk` (`warehouse_id`),
  KEY `parcels_users_user_id_fk` (`customer_id`),
  KEY `parcels_users_user_id_fk_2` (`employee_id`),
  CONSTRAINT `parcels_categories_primary_key_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`primary_key`),
  CONSTRAINT `parcels_shipments_primary_key_fk` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`primary_key`),
  CONSTRAINT `parcels_users_user_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `parcels_users_user_id_fk_2` FOREIGN KEY (`employee_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `parcels_warehouses_primary_key_fk` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`primary_key`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.parcels: ~14 rows (approximately)
/*!40000 ALTER TABLE `parcels` DISABLE KEYS */;
INSERT INTO `parcels` (`parcel_id`, `warehouse_id`, `weight`, `category_id`, `customer_id`, `shipment_id`, `employee_id`) VALUES
	(3, 1, 11, 1, 1, 2, 2),
	(4, 1, 30, 1, 1, 3, 2),
	(5, 2, 55, 1, 1, 2, 2),
	(6, 1, 667, 1, 1, 2, 2),
	(7, 1, 2, 2, 1, 2, 2),
	(8, 1, 67, 1, 1, 2, 2),
	(9, 1, 12, 2, 1, 2, 2),
	(10, 1, 120, 2, 1, 2, 2),
	(11, 1, 100, 2, 1, 2, 2),
	(12, 2, 500, 1, 1, 2, 2),
	(21, 3, 34, 1, 1, 10, 3),
	(22, 4, 567, 2, 6, 11, 4),
	(23, 5, 888, 1, 6, 12, 5),
	(24, 6, 950, 1, 6, 13, 5);
/*!40000 ALTER TABLE `parcels` ENABLE KEYS */;

-- Dumping structure for table deliverit.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(15) NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `roles_type_uindex` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `type`) VALUES
	(1, 'Customer'),
	(2, 'Employee');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table deliverit.shipments
DROP TABLE IF EXISTS `shipments`;
CREATE TABLE IF NOT EXISTS `shipments` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `departure_date` date DEFAULT NULL,
  `arrival_date` date DEFAULT NULL,
  `place_of_departure` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`primary_key`),
  KEY `shipments_employees_primary_key_fk` (`employee_id`),
  KEY `shipments_warehouses_primary_key_fk_2` (`place_of_departure`),
  CONSTRAINT `shipments_users_user_id_fk` FOREIGN KEY (`employee_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `shipments_warehouses_primary_key_fk_2` FOREIGN KEY (`place_of_departure`) REFERENCES `warehouses` (`primary_key`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.shipments: ~16 rows (approximately)
/*!40000 ALTER TABLE `shipments` DISABLE KEYS */;
INSERT INTO `shipments` (`primary_key`, `departure_date`, `arrival_date`, `place_of_departure`, `employee_id`) VALUES
	(2, '2021-03-24', '2021-03-31', 1, 2),
	(3, '2021-01-11', '2021-04-27', 1, 2),
	(4, '2021-02-09', '2021-02-01', 1, 2),
	(5, NULL, NULL, 2, 2),
	(7, NULL, NULL, 2, 2),
	(8, NULL, NULL, 2, 2),
	(9, NULL, NULL, 2, 2),
	(10, '2021-03-01', '2021-03-16', 3, 3),
	(11, '2021-03-03', '2021-03-15', 4, 3),
	(12, '2021-03-01', '2021-03-31', 5, 3),
	(13, '2021-03-02', '2021-08-18', 6, 4),
	(14, NULL, NULL, 7, 4),
	(15, NULL, NULL, 8, 5),
	(16, NULL, NULL, 9, 5),
	(17, NULL, NULL, 11, 4),
	(18, NULL, NULL, 11, 4);
/*!40000 ALTER TABLE `shipments` ENABLE KEYS */;

-- Dumping structure for table deliverit.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  `first_name` varchar(15) NOT NULL,
  `last_name` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  KEY `users_addresses_primary_key_fk` (`address_id`),
  CONSTRAINT `users_addresses_primary_key_fk` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`primary_key`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.users: ~6 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `password`, `first_name`, `last_name`, `email`, `address_id`) VALUES
	(1, 'vic', 'vvv', 'Victor', 'Valtchev', 'vic.valtchev@gmail.com', 1),
	(2, 'nas', 'nnn', 'Atanas', 'Doychev', 'a.d@gmail.com', NULL),
	(3, 'tod', 'ttt', 'Thomas', 'Muller', 't.m@gmail.com', NULL),
	(4, 'jack', 'jjj', 'Jack', 'Richard', 'j.r@gmail.com', NULL),
	(5, 'tim', 'ttt', 'Tim', 'TatMan', 't.t@gmail.com', NULL),
	(6, 'maria', 'mmm', 'Maria', 'DB', 'm.d@gmail.com', 5);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table deliverit.user_parcels
DROP TABLE IF EXISTS `user_parcels`;
CREATE TABLE IF NOT EXISTS `user_parcels` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `parcel_id` int(11) NOT NULL,
  PRIMARY KEY (`primary_key`),
  KEY `user_parcels_parcels_parcel_id_fk` (`parcel_id`),
  KEY `user_parcels_users_user_id_fk` (`user_id`),
  CONSTRAINT `user_parcels_parcels_parcel_id_fk` FOREIGN KEY (`parcel_id`) REFERENCES `parcels` (`parcel_id`),
  CONSTRAINT `user_parcels_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.user_parcels: ~13 rows (approximately)
/*!40000 ALTER TABLE `user_parcels` DISABLE KEYS */;
INSERT INTO `user_parcels` (`primary_key`, `user_id`, `parcel_id`) VALUES
	(1, 1, 3),
	(2, 1, 4),
	(3, 6, 5),
	(4, 1, 6),
	(5, 1, 7),
	(6, 1, 8),
	(7, 6, 9),
	(8, 1, 10),
	(9, 1, 11),
	(10, 6, 12),
	(11, 6, 22),
	(12, 6, 23),
	(13, 6, 24);
/*!40000 ALTER TABLE `user_parcels` ENABLE KEYS */;

-- Dumping structure for table deliverit.user_roles
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`primary_key`),
  KEY `user_roles_roles_role_id_fk` (`role_id`),
  KEY `user_roles_users_user_id_fk` (`user_id`),
  CONSTRAINT `user_roles_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `user_roles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.user_roles: ~6 rows (approximately)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`user_id`, `role_id`, `primary_key`) VALUES
	(1, 1, 1),
	(2, 2, 2),
	(3, 2, 3),
	(4, 2, 4),
	(5, 2, 5),
	(6, 1, 6);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

-- Dumping structure for table deliverit.warehouses
DROP TABLE IF EXISTS `warehouses`;
CREATE TABLE IF NOT EXISTS `warehouses` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) NOT NULL,
  PRIMARY KEY (`primary_key`),
  KEY `warehouses_addresses_primary_key_fk` (`address_id`),
  CONSTRAINT `warehouses_addresses_primary_key_fk` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`primary_key`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.warehouses: ~11 rows (approximately)
/*!40000 ALTER TABLE `warehouses` DISABLE KEYS */;
INSERT INTO `warehouses` (`primary_key`, `address_id`) VALUES
	(1, 1),
	(3, 2),
	(2, 3),
	(4, 4),
	(5, 5),
	(6, 6),
	(7, 7),
	(8, 8),
	(9, 9),
	(10, 10),
	(11, 11);
/*!40000 ALTER TABLE `warehouses` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
