-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for deliverit
DROP DATABASE IF EXISTS `deliverit`;
CREATE DATABASE IF NOT EXISTS `deliverit` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `deliverit`;

-- Dumping structure for table deliverit.addresses
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(30) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`primary_key`),
  UNIQUE KEY `addresses_address_uindex` (`address`),
  KEY `addresses_cities_primary_key_fk` (`city_id`),
  CONSTRAINT `addresses_cities_primary_key_fk` FOREIGN KEY (`city_id`) REFERENCES `cities` (`primary_key`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliverit.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(30) NOT NULL,
  PRIMARY KEY (`primary_key`),
  UNIQUE KEY `categories_category_uindex` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliverit.cities
DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`primary_key`),
  UNIQUE KEY `cities_name_uindex` (`name`),
  KEY `cities_countries_primary_key_fk` (`country_id`),
  CONSTRAINT `cities_countries_primary_key_fk` FOREIGN KEY (`country_id`) REFERENCES `countries` (`primary_key`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliverit.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`primary_key`),
  UNIQUE KEY `countries_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliverit.parcels
DROP TABLE IF EXISTS `parcels`;
CREATE TABLE IF NOT EXISTS `parcels` (
  `parcel_id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_id` int(11) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `shipment_id` int(11) DEFAULT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`parcel_id`),
  KEY `parcels_categories_primary_key_fk` (`category_id`),
  KEY `parcels_shipments_primary_key_fk` (`shipment_id`),
  KEY `parcels_warehouses_primary_key_fk` (`warehouse_id`),
  KEY `parcels_users_user_id_fk` (`customer_id`),
  KEY `parcels_users_user_id_fk_2` (`employee_id`),
  CONSTRAINT `parcels_categories_primary_key_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`primary_key`),
  CONSTRAINT `parcels_shipments_primary_key_fk` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`primary_key`),
  CONSTRAINT `parcels_users_user_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `parcels_users_user_id_fk_2` FOREIGN KEY (`employee_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `parcels_warehouses_primary_key_fk` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`primary_key`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliverit.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(15) NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `roles_type_uindex` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliverit.shipments
DROP TABLE IF EXISTS `shipments`;
CREATE TABLE IF NOT EXISTS `shipments` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `departure_date` date DEFAULT NULL,
  `arrival_date` date DEFAULT NULL,
  `place_of_departure` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`primary_key`),
  KEY `shipments_employees_primary_key_fk` (`employee_id`),
  KEY `shipments_warehouses_primary_key_fk_2` (`place_of_departure`),
  CONSTRAINT `shipments_users_user_id_fk` FOREIGN KEY (`employee_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `shipments_warehouses_primary_key_fk_2` FOREIGN KEY (`place_of_departure`) REFERENCES `warehouses` (`primary_key`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliverit.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(15) NOT NULL,
  `last_name` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  KEY `users_addresses_primary_key_fk` (`address_id`),
  CONSTRAINT `users_addresses_primary_key_fk` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`primary_key`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliverit.user_parcels
DROP TABLE IF EXISTS `user_parcels`;
CREATE TABLE IF NOT EXISTS `user_parcels` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `parcel_id` int(11) NOT NULL,
  PRIMARY KEY (`primary_key`),
  KEY `user_parcels_parcels_parcel_id_fk` (`parcel_id`),
  KEY `user_parcels_users_user_id_fk` (`user_id`),
  CONSTRAINT `user_parcels_parcels_parcel_id_fk` FOREIGN KEY (`parcel_id`) REFERENCES `parcels` (`parcel_id`),
  CONSTRAINT `user_parcels_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliverit.user_roles
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`primary_key`),
  KEY `user_roles_roles_role_id_fk` (`role_id`),
  KEY `user_roles_users_user_id_fk` (`user_id`),
  CONSTRAINT `user_roles_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `user_roles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliverit.warehouses
DROP TABLE IF EXISTS `warehouses`;
CREATE TABLE IF NOT EXISTS `warehouses` (
  `primary_key` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) NOT NULL,
  PRIMARY KEY (`primary_key`),
  KEY `warehouses_addresses_primary_key_fk` (`address_id`),
  CONSTRAINT `warehouses_addresses_primary_key_fk` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`primary_key`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
