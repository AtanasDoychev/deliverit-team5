
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дъмп на структурата на БД deliverit
DROP DATABASE IF EXISTS `deliverit`;
CREATE DATABASE IF NOT EXISTS `deliverit` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `deliverit`;

-- Дъмп структура за таблица deliverit.addresses
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
    `primary_key` int(11) NOT NULL AUTO_INCREMENT,
    `address` varchar(30) NOT NULL,
    `city_id` int(11) NOT NULL,
    PRIMARY KEY (`primary_key`),
    UNIQUE KEY `addresses_address_uindex` (`address`),
    KEY `addresses_cities_primary_key_fk` (`city_id`),
    CONSTRAINT `addresses_cities_primary_key_fk` FOREIGN KEY (`city_id`) REFERENCES `cities` (`primary_key`)
    ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица deliverit.addresses: ~4 rows (приблизително)
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` (`primary_key`, `address`, `city_id`) VALUES
(1, 'Glavnata 2', 1),
(2, 'Morska 4', 2),
(3, 'st.Rogers', 3),
(4, 'st.Clay', 4);
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;

-- Дъмп структура за таблица deliverit.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
    `primary_key` int(11) NOT NULL AUTO_INCREMENT,
    `category` varchar(30) NOT NULL,
    PRIMARY KEY (`primary_key`),
    UNIQUE KEY `categories_category_uindex` (`category`)
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица deliverit.categories: ~2 rows (приблизително)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`primary_key`, `category`) VALUES
(2, 'Clothes'),
(1, 'Electronics');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Дъмп структура за таблица deliverit.cities
DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
    `primary_key` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(30) NOT NULL,
    `country_id` int(11) NOT NULL,
    PRIMARY KEY (`primary_key`),
    UNIQUE KEY `cities_name_uindex` (`name`),
    KEY `cities_countries_primary_key_fk` (`country_id`),
    CONSTRAINT `cities_countries_primary_key_fk` FOREIGN KEY (`country_id`) REFERENCES `countries` (`primary_key`)
    ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица deliverit.cities: ~4 rows (приблизително)
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` (`primary_key`, `name`, `country_id`) VALUES
(1, 'Sofia', 1),
(2, 'Varna', 1),
(3, 'London', 2),
(4, 'Birmingham', 2);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;

-- Дъмп структура за таблица deliverit.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
    `primary_key` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(30) NOT NULL,
    PRIMARY KEY (`primary_key`),
    UNIQUE KEY `countries_name_uindex` (`name`)
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица deliverit.countries: ~2 rows (приблизително)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`primary_key`, `name`) VALUES
(1, 'Bulgaria'),
(2, 'UK');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Дъмп структура за таблица deliverit.parcels
DROP TABLE IF EXISTS `parcels`;
CREATE TABLE IF NOT EXISTS `parcels` (
    `parcel_id` int(11) NOT NULL AUTO_INCREMENT,
    `warehouse_id` int(11) NOT NULL,
    `weight` int(11) DEFAULT NULL,
    `category_id` int(11) NOT NULL,
    `customer_id` int(11) NOT NULL,
    `shipment_id` int(11) DEFAULT NULL,
    `employee_id` int(11) NOT NULL,
    PRIMARY KEY (`parcel_id`),
    KEY `parcels_categories_primary_key_fk` (`category_id`),
    KEY `parcels_shipments_primary_key_fk` (`shipment_id`),
    KEY `parcels_warehouses_primary_key_fk` (`warehouse_id`),
    KEY `parcels_users_user_id_fk` (`customer_id`),
    KEY `parcels_users_user_id_fk_2` (`employee_id`),
    CONSTRAINT `parcels_categories_primary_key_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`primary_key`),
    CONSTRAINT `parcels_shipments_primary_key_fk` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`primary_key`),
    CONSTRAINT `parcels_users_user_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `users` (`user_id`),
    CONSTRAINT `parcels_users_user_id_fk_2` FOREIGN KEY (`employee_id`) REFERENCES `users` (`user_id`),
    CONSTRAINT `parcels_warehouses_primary_key_fk` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`primary_key`)
    ) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица deliverit.parcels: ~11 rows (приблизително)
/*!40000 ALTER TABLE `parcels` DISABLE KEYS */;
INSERT INTO `parcels` (`parcel_id`, `warehouse_id`, `weight`, `category_id`, `customer_id`, `shipment_id`, `employee_id`) VALUES
(3, 1, 11, 1, 1, 2, 2),
(4, 1, 30, 1, 1, 3, 2),
(5, 2, 55, 1, 1, 2, 2),
(6, 1, 667, 1, 1, 2, 2),
(7, 1, 2, 2, 1, 2, 2),
(8, 1, 67, 1, 1, 2, 2),
(9, 1, 12, 2, 1, 2, 2),
(10, 1, 0, 2, 1, 2, 2),
(11, 1, -2, 2, 1, 2, 2),
(12, 2, 0, 1, 1, 2, 2);
/*!40000 ALTER TABLE `parcels` ENABLE KEYS */;

-- Дъмп структура за таблица deliverit.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
    `role_id` int(11) NOT NULL AUTO_INCREMENT,
    `type` varchar(15) NOT NULL,
    PRIMARY KEY (`role_id`),
    UNIQUE KEY `roles_type_uindex` (`type`)
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица deliverit.roles: ~2 rows (приблизително)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `type`) VALUES
(1, 'Customer'),
(2, 'Employee');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Дъмп структура за таблица deliverit.shipments
DROP TABLE IF EXISTS `shipments`;
CREATE TABLE IF NOT EXISTS `shipments` (
    `primary_key` int(11) NOT NULL AUTO_INCREMENT,
    `departure_date` date,
    `arrival_date` date ,
    `place_of_departure` int(11) NOT NULL,
    `employee_id` int(11) NOT NULL,
    PRIMARY KEY (`primary_key`),
    KEY `shipments_employees_primary_key_fk` (`employee_id`),
    KEY `shipments_warehouses_primary_key_fk_2` (`place_of_departure`),
    CONSTRAINT `shipments_users_user_id_fk` FOREIGN KEY (`employee_id`) REFERENCES `users` (`user_id`),
    CONSTRAINT `shipments_warehouses_primary_key_fk_2` FOREIGN KEY (`place_of_departure`) REFERENCES `warehouses` (`primary_key`)
    ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица deliverit.shipments: ~3 rows (приблизително)
/*!40000 ALTER TABLE `shipments` DISABLE KEYS */;
INSERT INTO `shipments` (`primary_key`, `departure_date`, `arrival_date`, `place_of_departure`, `employee_id`) VALUES
(2, '2021-03-28', '2022-03-07', 1, 2),
(3, '2021-01-11', '2021-04-27', 1, 2),
(4, '2021-02-09', '2021-02-01', 1, 2);
/*!40000 ALTER TABLE `shipments` ENABLE KEYS */;

-- Дъмп структура за таблица deliverit.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
    `user_id` int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(20) NOT NULL,
    `password` varchar(30) NOT NULL,
    `first_name` varchar(15) NOT NULL,
    `last_name` varchar(15) NOT NULL,
    `email` varchar(30) NOT NULL,
    `address_id` int(11) DEFAULT NULL,
    PRIMARY KEY (`user_id`),
    UNIQUE KEY `users_email_uindex` (`email`),
    KEY `users_addresses_primary_key_fk` (`address_id`),
    CONSTRAINT `users_addresses_primary_key_fk` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`primary_key`)
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица deliverit.users: ~2 rows (приблизително)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `password`, `first_name`, `last_name`, `email`, `address_id`) VALUES
(1, 'vic', 'vvv', 'Victor', 'Valtchev', 'vic.valtchev@gmail.com', 1),
(2, 'nas', 'nnn', 'Atanas', 'Doychev', 'a.d@gmail.com', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Дъмп структура за таблица deliverit.user_parcels
DROP TABLE IF EXISTS `user_parcels`;
CREATE TABLE IF NOT EXISTS `user_parcels` (
    `primary_key` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `parcel_id` int(11) NOT NULL,
    PRIMARY KEY (`primary_key`),
    KEY `user_parcels_parcels_parcel_id_fk` (`parcel_id`),
    KEY `user_parcels_users_user_id_fk` (`user_id`),
    CONSTRAINT `user_parcels_parcels_parcel_id_fk` FOREIGN KEY (`parcel_id`) REFERENCES `parcels` (`parcel_id`),
    CONSTRAINT `user_parcels_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица deliverit.user_parcels: ~10 rows (приблизително)
/*!40000 ALTER TABLE `user_parcels` DISABLE KEYS */;
INSERT INTO `user_parcels` (`primary_key`, `user_id`, `parcel_id`) VALUES
(1, 1, 3),
(2, 1, 4),
(3, 1, 5),
(4, 1, 6),
(5, 1, 7),
(6, 1, 8),
(7, 1, 9),
(8, 1, 10),
(9, 1, 11),
(10, 1, 12);
/*!40000 ALTER TABLE `user_parcels` ENABLE KEYS */;

-- Дъмп структура за таблица deliverit.user_roles
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
    `user_id` int(11) DEFAULT NULL,
    `role_id` int(11) DEFAULT NULL,
    `primary_key` int(11) NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`primary_key`),
    KEY `user_roles_roles_role_id_fk` (`role_id`),
    KEY `user_roles_users_user_id_fk` (`user_id`),
    CONSTRAINT `user_roles_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
    CONSTRAINT `user_roles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица deliverit.user_roles: ~2 rows (приблизително)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`user_id`, `role_id`, `primary_key`) VALUES
(1, 1, 1),
(2, 2, 2);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

-- Дъмп структура за таблица deliverit.warehouses
DROP TABLE IF EXISTS `warehouses`;
CREATE TABLE IF NOT EXISTS `warehouses` (
    `primary_key` int(11) NOT NULL AUTO_INCREMENT,
    `address_id` int(11) NOT NULL,
    PRIMARY KEY (`primary_key`),
    KEY `warehouses_addresses_primary_key_fk` (`address_id`),
    CONSTRAINT `warehouses_addresses_primary_key_fk` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`primary_key`)
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица deliverit.warehouses: ~2 rows (приблизително)
/*!40000 ALTER TABLE `warehouses` DISABLE KEYS */;
INSERT INTO `warehouses` (`primary_key`, `address_id`) VALUES
(1, 1),
(2, 3);
/*!40000 ALTER TABLE `warehouses` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
