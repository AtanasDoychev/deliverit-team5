package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.OrderOfOperationsException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.Shipment;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.models.Warehouse;
import com.telerikaacademy.demo.repository.ShipmentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.text.ParseException;
import java.util.Optional;
import java.util.Set;

import static com.telerikaacademy.demo.Helpers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ShipmentServiceImplTests {


    @Mock
    ShipmentRepository mockShipmentRepository;

    @InjectMocks
    ShipmentServiceImpl mockShipmentService;

    @Test
    public void getAll_Should_CallService() {
        User mockEmployee = createMockEmployee();
        Optional<String> mockWarehouseID = Optional.empty();
        Optional<Integer> mockShipmentID = Optional.empty();
        mockShipmentService.getAll(mockEmployee, mockWarehouseID, mockShipmentID);
        Mockito.verify(mockShipmentRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getByShipmentId_Should_CallService() {
        User mockEmployee = createMockEmployee();
        String mockID = "1";
        Optional<String> mockWarehouseID = Optional.of(mockID);
        Optional<Integer> mockShipmentID = Optional.empty();
        mockShipmentService.getAll(mockEmployee, mockWarehouseID, mockShipmentID);
        Mockito.verify(mockShipmentRepository,
                Mockito.times(1)).getAllFilteredByWarehouse(Integer.parseInt(mockID));
    }

    @Test
    public void getByWarehouseId_Should_CallService() {
        User mockEmployee = createMockEmployee();
        Optional<String> mockWarehouseID = Optional.empty();
        Integer mockID = 1;
        Optional<Integer> mockShipmentID = Optional.of(mockID);
        mockShipmentService.getAll(mockEmployee, mockWarehouseID, mockShipmentID);
        Mockito.verify(mockShipmentRepository,
                Mockito.times(1)).getShipmentByID(mockID);
    }

    @Test
    public void getByWarehouseIdAndShipmentID_Should_Throw() {
        User mockEmployee = createMockEmployee();
        String mockID2 = "1";
        Optional<String> mockWarehouseID = Optional.of(mockID2);
        Integer mockID = 1;
        Optional<Integer> mockShipmentID = Optional.of(mockID);
        Assertions.assertThrows(OrderOfOperationsException.class,
                () -> mockShipmentService.getAll(mockEmployee,mockWarehouseID,mockShipmentID));
    }

    @Test
    public void getCustomerShipments_Should_CallService() {
        User mockEmployee = createMockEmployee();
        int id = 1;
        mockShipmentService.getCustomerShipments(mockEmployee,id);
        Mockito.verify(mockShipmentRepository,
                Mockito.times(1)).getCustomerShipments(id);
    }

    @Test
    public void getCustomerShipments_Throw() {
        User mockCustomer = createMockCustomer();
        mockCustomer.setId(10);
        int id = 1;
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockShipmentService.getCustomerShipments(mockCustomer, id));
    }

    @Test
    public void create_Should_CallService() {
        User mockEmployee = createMockEmployee();
        Shipment mockShipment = createMockShipment();
        mockShipmentService.create(mockShipment,mockEmployee);
        Mockito.verify(mockShipmentRepository,
                Mockito.times(1)).create(mockShipment);
    }

    @Test
    public void create_Should_Throw() {
        User mockCustomer = createMockCustomer();
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockShipmentService.verifyIfEmployee(mockCustomer));
    }

    @Test
    public void update_Should_CallService() {
        User mockEmployee = createMockEmployee();
        Shipment mockShipment = createMockShipment();
        mockShipmentService.update(mockShipment,mockEmployee);
        Mockito.verify(mockShipmentRepository,Mockito.times(1)).update(mockShipment);
    }

    @Test
    public void update_Should_Throw() {
        User mockEmployee = createMockEmployee();
        Shipment shipment = new Shipment();
        doThrow(new EntityNotFoundException("test exception"))
                .when(mockShipmentRepository).update(shipment);
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockShipmentService.update(shipment, mockEmployee));
    }


    @Test
    public void verifyIfEmployee_Should_Throw() {
        User mockUser = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockShipmentService.verifyIfEmployee(mockUser));
    }

    @Test
    public void verifyIfCustomer_WhenEmployee_Should_Pass() throws ParseException {
        User mockUser = createMockEmployee();
        mockUser.setRoles(Set.of(createMockRoleCustomer(),createMockRoleEmployee()));
        mockShipmentService.verifyIfCustomer(mockUser);
    }

    @Test
    public void getAllWithStatus_Should_Call_Service() {
        User mockUser = createMockEmployee();
        mockShipmentService.getAllWithStatus(mockUser,"Preparing");
        Mockito.verify(mockShipmentRepository, Mockito.times(1)).getAllWithStatus("Preparing");
    }

    @Test
    public void getAllFilteredByWarehouse() {
        User mockUser = createMockEmployee();
        Shipment mockShipment = createMockShipment();
        Warehouse mockWarehouse = createMockWarehouse();
        mockWarehouse.setPrimary_key(1);
        mockShipment.setPlace_of_departure(mockWarehouse);
        mockShipmentService.getAllFilteredByWarehouse(mockUser,1);
        Mockito.verify(mockShipmentRepository, Mockito.times(1))
                .getAllFilteredByWarehouse(1);
    }

    @Test
    public void getNextArrivingShipment() {
        User mockUser = createMockEmployee();
        Shipment mockShipment = createMockShipment();
        mockShipmentService.getNextArrivingShipment(mockUser);
        Mockito.verify(mockShipmentRepository, Mockito.times(1)).getNextArrivingShipment();
    }


    @Test
    public void delete_Should_CallService() {
        User mockUser = createMockEmployee();
        Shipment mockShipment = createMockShipment();
        mockShipment.setId(1);
        mockShipmentService.delete(mockUser, 1);
        Mockito.when(mockShipmentRepository.getById(Mockito.anyInt()))
                  .thenReturn(mockShipment);
        mockShipmentService.delete(mockUser, 1);
        Mockito.verify(mockShipmentRepository, Mockito.times(1)).delete(mockShipment);
    }
}
