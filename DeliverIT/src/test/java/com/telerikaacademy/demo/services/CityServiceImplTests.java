package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.repository.CityRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CityServiceImplTests {

    @Mock
    CityRepository mockCityRepository;

    @InjectMocks
    CityServiceImpl service;

    @Test
    public void getAll_Should_CallService() {
        service.getAll();
        Mockito.verify(mockCityRepository,
                Mockito.times(1)).getAll();
    }
}
