package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;

import static com.telerikaacademy.demo.Helpers.createMockCustomer;
import static com.telerikaacademy.demo.Helpers.createMockEmployee;

@ExtendWith(MockitoExtension.class)
public class AuthorizationHelperTests {

    @InjectMocks
    AuthorizationHelper helper;


    @Test
    public void verifyIfEmployee_Should_Throw()  {
        User mockUser = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> helper.verifyIfEmployee(mockUser, "Mock"));
    }

    @Test
    public void verifyIfCustomer_Should_Throw() {
        User mockUser = createMockEmployee();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> helper.verifyIfCustomer(mockUser, "Mock"));
    }
}
