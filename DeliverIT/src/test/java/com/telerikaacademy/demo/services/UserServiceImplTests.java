package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.repository.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    UserServiceImpl mockUserService;

   @Test
   public void getAllByCount_Should_CallService() {
       Mockito.when(mockUserRepository.getAllCustomersCount()).thenReturn("Mock");

       mockUserService.getAllCustomersCount();

       Mockito.verify(mockUserRepository,
               Mockito.times(1)).getAllCustomersCount();
   }
//
 //   @Test
 //   public void getAll_Should_CallService() {
 //       User mockEmployee = createMockEmployee();
 //       mockUserService.getAllUsers(mockEmployee);
 //       Mockito.verify(mockUserRepository,Mockito.times(1)).getAll();
 //   }
//
 //   @Test
 //   public void create_Should_CallService() {
 //       User mockCustomer = createMockCustomer();
 //       mockUserService.createUser(mockCustomer);
 //       Mockito.verify(mockUserRepository,
 //               Mockito.times(1)).createUser(mockCustomer);
 //   }
}
