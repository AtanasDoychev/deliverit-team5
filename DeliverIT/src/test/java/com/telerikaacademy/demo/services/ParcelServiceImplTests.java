package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.Parcels;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.repository.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;

import static com.telerikaacademy.demo.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ParcelServiceImplTests {

    @Mock
    ParcelRepository mockParcelRepository;

    @InjectMocks
    ParcelServiceImpl service;


    @Test
    public void getAll_Should_CallService() {
        User mockUser = createMockEmployee();
        service.getAll(mockUser);
        Mockito.verify(mockParcelRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void create_Should_CallService() {
        Parcels mockParcel = createMockParcel();
        User mockUser = createMockEmployee();

        service.create(mockParcel, mockUser);
        Mockito.verify(mockParcelRepository,
                Mockito.times(1)).create(mockParcel);
    }

    @Test
    public void update_should_CallService_When_ParcelNotInRepository() {
        Parcels mockParcel = createMockParcel();
        User mockUser = createMockEmployee();

        Mockito.when(mockParcelRepository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);
        service.update(mockParcel, mockUser);

        Mockito.verify(mockParcelRepository,
                Mockito.times(1)).update(mockParcel);
    }

    //todo works but not sure if correct
    @Test
    public void delete_Should_CallService() {
        Parcels mockParcel = createMockParcel();
        User mockUser = createMockEmployee();

        Mockito.when(mockParcelRepository.getById(Mockito.anyInt()))
                .thenReturn(mockParcel);

        service.delete(mockUser, 1);

        Mockito.verify(mockParcelRepository,
                Mockito.times(1)).delete(mockParcel);
    }

    @Test
    public void filterByWeight_Should_CallService() {
        User mockUser = createMockEmployee();

        service.filterByWeight(mockUser, 1, 5);
        Mockito.verify(mockParcelRepository,
                Mockito.times(1)).filterByWeight(1, 5);
    }

    //todo test for filter

    @Test
    public void getCustomerOwnParcels_Should_CallService() {
        User mockUser = createMockCustomer();

        service.getCustomerOwnParcels(mockUser, 1);
        Mockito.verify(mockParcelRepository,
                Mockito.times(1)).getCustomerOwnParcels(1);
    }

    @Test
    public void sortByWeight_Should_CallService() {
        User mockUser = createMockEmployee();

        service.sortByWeight(mockUser);
        Mockito.verify(mockParcelRepository,
                Mockito.times(1)).sortByWeight();
    }

    @Test
    public void sortByDate_Should_CallService() {
        User mockUser = createMockEmployee();

        service.sortByDate(mockUser);
        Mockito.verify(mockParcelRepository,
                Mockito.times(1)).sortByDate();
    }

    @Test
    public void sortByWeightAndDate_Should_CallService() {
        User mockUser = createMockEmployee();

        service.sortByWeightAndDate(mockUser);
        Mockito.verify(mockParcelRepository,
                Mockito.times(1)).sortByWeightAndDate();
    }
}