package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.models.Warehouse;
import com.telerikaacademy.demo.repository.WarehouseRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikaacademy.demo.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class WarehouseServiceImplTests {

    @Mock
    WarehouseRepository mockWarehouseRepository;

    @InjectMocks
    WarehouseServiceImpl service;

    @Test
    public void getAll_Should_CallService() {
        service.getAll();
        Mockito.verify(mockWarehouseRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_CallService() {
        User mockUser = createMockEmployee();
        service.getById(mockUser, 1);
        Mockito.verify(mockWarehouseRepository,
                Mockito.times(1)).getById(1);
    }


    @Test
    public void create_Should_CallRepository() {
        //
        User mockUser = createMockEmployee();
        Warehouse mockWarehouse = createMockWarehouse();

        Mockito.when(mockWarehouseRepository.getByAddress(
                mockWarehouse.getAddress()))
                .thenThrow(EntityNotFoundException.class);
        //
        service.create(mockUser, mockWarehouse);

        //
        Mockito.verify(mockWarehouseRepository,
                Mockito.times(1)).create(mockWarehouse);
    }

    @Test
    public void delete_Should_CallService() {
        User mockUser = createMockEmployee();
        service.delete(mockUser, 1);
        Mockito.verify(mockWarehouseRepository,
                Mockito.times(1)).delete(1);
    }
}
