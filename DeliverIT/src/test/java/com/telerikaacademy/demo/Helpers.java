package com.telerikaacademy.demo;

import com.telerikaacademy.demo.models.*;
import org.springframework.http.HttpHeaders;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Set;


public class Helpers {

    public static Shipment createMockShipment() {
        var mockShipment = new Shipment();
        mockShipment.setId(1);
        mockShipment.setDeparture_date(Timestamp.valueOf("2021-03-03 01:01:01"));
        mockShipment.setArrival_date(Timestamp.valueOf("2022-03-03 01:01:01"));
        mockShipment.setPlace_of_departure(createMockWarehouse());
        mockShipment.setEmployeeId(1);
        return mockShipment;
    }

    public static Warehouse createMockWarehouse() {
        var mockWarehouse = new Warehouse();
        mockWarehouse.setAddress(createMockAddress());
        mockWarehouse.setPrimary_key(1);
        return mockWarehouse;
    }

    public static Address createMockAddress() {
        var mockAddress = new Address();
        mockAddress.setAddress("Dondukov 2");
        mockAddress.setId(1);
        mockAddress.setCity(createMockCity());
        return mockAddress;
    }

    public static City createMockCity() {
        var mockCity = new City();
        mockCity.setId(1);
        mockCity.setName("Sofia");
        mockCity.setCountry(createMockCountry());
        return mockCity;
    }

    public static Country createMockCountry() {
        var mockCountry = new Country();
        mockCountry.setName("Bulgaria");
        mockCountry.setId(1);
        return mockCountry;
    }

//    public static Date createMockDate(Integer integer) {
//        Integer value = integer;
//        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyyMMdd");
//        Date date = originalFormat.parse(value.toString());
//        return date;
//    }

    public static HttpHeaders createMockHttpHeader() throws ParseException {
        var mockHeader = new HttpHeaders();
        mockHeader.set("Authorization", "a.d@gmail.com");
        return mockHeader;
    }

    public static Parcels createMockParcel() {
        var mockParcel = new Parcels();
        mockParcel.setId(1);
        mockParcel.setWeight(22.00);
        mockParcel.setWarehouse(createMockWarehouse());
        mockParcel.setCategory(createMockCategory());
        mockParcel.setCustomer(createMockCustomer());
        mockParcel.setEmployee(createMockEmployee());
        return mockParcel;
    }

    public static Categories createMockCategory() {
        var mockCategory = new Categories();
        mockCategory.setId(1);
        mockCategory.setCategory("Goods");
        return mockCategory;
    }

    public static User createMockCustomer() {
        var mockCustomer = new User();
        mockCustomer.setId(1);
        mockCustomer.setFirstName("Elon");
        mockCustomer.setLastName("Musk");
        mockCustomer.setEmail("e.muskito@spacex.com");
        mockCustomer.setAddress(createMockAddress());
        mockCustomer.setRoles(Set.of(createMockRoleCustomer()));
        mockCustomer.setListOfParcels(null);
        return mockCustomer;
    }

    public static User createMockEmployee() {
        User mockEmployee = new User();
        mockEmployee.setId(2);
        mockEmployee.setFirstName("Grigor");
        mockEmployee.setLastName("Dimitrov");
        mockEmployee.setEmail("grisho.cool@abv.bg");
        mockEmployee.setAddress(createMockAddress());
        mockEmployee.setRoles(Set.of(createMockRoleEmployee()));
        mockEmployee.setListOfParcels(null);
        return mockEmployee;
    }

    public static Role createMockRoleCustomer() {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setRole("Customer");
        return mockRole;
    }

    public static Role createMockRoleEmployee() {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setRole("Employee");
        return mockRole;
    }

}
