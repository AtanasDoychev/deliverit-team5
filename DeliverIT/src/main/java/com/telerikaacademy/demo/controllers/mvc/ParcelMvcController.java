package com.telerikaacademy.demo.controllers.mvc;

import com.telerikaacademy.demo.controllers.AuthenticationHelper;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.*;
import com.telerikaacademy.demo.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/parcels")
public class ParcelMvcController {

    private final ParcelService parcelService;
    private final WarehouseService warehouseService;
    private final CategoryService categoryService;
    private final UserService userService;
    private final ShipmentService shipmentService;
    private final ParcelMapper parcelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ParcelMvcController(ParcelService parcelService,
                               WarehouseService warehouseService,
                               CategoryService categoryService,
                               UserService userService, ShipmentService shipmentService, ParcelMapper parcelMapper, AuthenticationHelper authenticationHelper) {
        this.parcelService = parcelService;
        this.warehouseService = warehouseService;
        this.categoryService = categoryService;
        this.userService = userService;
        this.shipmentService = shipmentService;
        this.parcelMapper = parcelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    //this 5 under are for the drop down menus in parcel-new.html
    @ModelAttribute("warehouses")
    public List<Warehouse> populateWarehouses() {
        return warehouseService.getAll();
    }

    @ModelAttribute("categories")
    public List<Categories> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("customers")
    public List<User> populateCustomers() {
        //todo temp user, to be removed
        User employee = userService.getUserByEmail("a.d@gmail.com");
        return userService.getAllCustomers(employee);
    }

    @ModelAttribute("employees")
    public List<User> populateEmployees() {
        //todo temp user, to be removed
        User employee = userService.getUserByEmail("a.d@gmail.com");
        return userService.getAllEmployees2(employee);
    }

    @ModelAttribute("shipments")
    public List<Shipment> populateShipments() {
        //todo temp user, to be removed
        User employee = userService.getUserByEmail("a.d@gmail.com");
        return shipmentService.getAll(employee, Optional.empty(), Optional.empty());
    }

    @GetMapping
    public String showAllParcels(HttpSession session, Model model) {

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!currentUser.isEmployee()) {
                return "notFound";
            }
        } catch (UnauthorizedOperationException e) {
            return "notFound";
        }

        model.addAttribute("parcels", parcelService.getAll(currentUser));
        return "parcels"; //dont change this is for all parcels
    }

    @GetMapping("/userParcels")
    public String showAllParcelsOfOneUser(HttpSession session, Model model) {

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!currentUser.isCustomer()) {
                return "notFound";
            }
        } catch (UnauthorizedOperationException e) {
            return "notFound";
        }

        model.addAttribute("oneUserParcels",
                parcelService.getCustomerOwnParcels(currentUser, currentUser.getId()));
        return "user-parcels";
    }

    @GetMapping("/{id}")
    public String showOneParcel(@PathVariable int id, HttpSession session, Model model) {
        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!currentUser.isEmployee()) {
                return "notFound";
            }
        } catch (UnauthorizedOperationException e) {
            return "notFound";
        }

        try {
            Parcels parcel = parcelService.getById(id, currentUser);
            model.addAttribute("parcel", parcel);
            return "parcel";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "notFound";
        }
    }

    @GetMapping("/new")
    public String showNewParcelPage(HttpSession session, Model model) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!currentUser.isEmployee()) {
                return "notFound";
            }
        } catch (UnauthorizedOperationException e) {
            return "notFound";
        }

        model.addAttribute("parcel", new ParcelsDto());
        return "parcel-new";
    }

    //  we can have duplicate parcels, no validation needed
    @PostMapping("/new")
    public String createParcel(@Valid @ModelAttribute("parcel") ParcelsDto parcelDto,
                               BindingResult errors,
                               HttpSession session,
                               Model model) {
        if (errors.hasErrors()) {
            return "parcel-new";
        }

        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!currentUser.isEmployee()) {
                return "notFound";
            }
        } catch (UnauthorizedOperationException e) {
            return "notFound";
        }

        Parcels parcels = parcelMapper.fromDto(parcelDto, currentUser.getId());
        parcelService.create(parcels, currentUser);
        return "redirect:/parcels";
    }

    @GetMapping("/{id}/update")
    public String showParcelEditPage(@PathVariable int id, HttpSession session, Model model) {
        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!currentUser.isEmployee()) {
                return "notFound";
            }
        } catch (UnauthorizedOperationException e) {
            return "notFound";
        }

        try {
            Parcels parcel = parcelService.getById(id, currentUser);
            ParcelsDto parcelDto = parcelMapper.toDto(parcel);
            model.addAttribute("parcelId", id);
            model.addAttribute("parcel", parcelDto);
            return "parcel-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "notFound";
        }
    }

    @PostMapping("/{id}/update")
    public String updateParcel(@PathVariable int id,
                               @Valid @ModelAttribute("parcel") ParcelsDto parcelsDto,
                               BindingResult errors, HttpSession session, Model model) {
        if (errors.hasErrors()) {
            return "parcel-update";
        }

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!currentUser.isEmployee()) {
                return "notFound";
            }
        } catch (UnauthorizedOperationException e) {
            return "notFound";
        }

        try {
            Parcels parcels = parcelMapper.fromDto(parcelsDto, currentUser.getId(), id);
            parcelService.update(parcels, currentUser);

            return "redirect:/parcels";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "notFound";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteParcel(@PathVariable int id, HttpSession session, Model model) {
//        if (errors.hasErrors()) {
//            return "parcels";
//        }

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!currentUser.isEmployee()) {
                return "notFound";
            }
        } catch (UnauthorizedOperationException e) {
            return "notFound";
        }

        try {
            parcelService.delete(currentUser, id);
            return "parcels";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "notFound";
        }
    }

    @GetMapping("/search")
    public String searchParcelsPage(HttpSession session, Model model) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!currentUser.isEmployee()) {
                return "notFound";
            }
        } catch (UnauthorizedOperationException e) {
            return "notFound";
        }

//        model.addAttribute("parcel", new ParcelsDto());
        return "parcelsJS";
    }

}
