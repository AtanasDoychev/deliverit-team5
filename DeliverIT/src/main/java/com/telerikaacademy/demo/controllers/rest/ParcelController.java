package com.telerikaacademy.demo.controllers.rest;

import com.telerikaacademy.demo.controllers.AuthenticationHelper;
import com.telerikaacademy.demo.exceptions.DuplicateEntityException;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.OrderOfOperationsException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.ParcelsDto;
import com.telerikaacademy.demo.models.Parcels;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.services.ParcelMapper;
import com.telerikaacademy.demo.services.ParcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/parcels")
public class ParcelController {

    private final ParcelService parcelService;
    private final ParcelMapper parcelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ParcelController(ParcelService parcelService,
                            ParcelMapper parcelMapper,
                            AuthenticationHelper authenticationHelper) {
        this.parcelService = parcelService;
        this.parcelMapper = parcelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Parcels> getAll(@RequestHeader HttpHeaders headers) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            return parcelService.getAll(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public Parcels create(@RequestHeader HttpHeaders headers,
                          @Valid @RequestBody ParcelsDto parcelsDto) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            Parcels parcels = parcelMapper.fromDto(parcelsDto, user.getId());
            parcelService.create(parcels, user);
            return parcels;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Parcels update(@RequestHeader HttpHeaders headers,
                          @PathVariable int id,
                          @Valid @RequestBody ParcelsDto parcelsDto) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            Parcels parcels = parcelMapper.fromDto(parcelsDto, user.getId(), id);
            parcelService.update(parcels, user);
            return parcels;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    //todo can we delete if parcels pointing at some warehouse, customer?
    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            parcelService.delete(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (OrderOfOperationsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filter/weight")
    //  api/parcels/filter/weight?min=10&max=50
    public List<Parcels> filterByWeight(@RequestHeader HttpHeaders headers,
                                        @RequestParam int min, int max) {

        if (min >= max) {
            throw new ResponseStatusException(HttpStatus.CONFLICT,
                    "Maximal value should not be smaller or equal to the minimal value.");
        }

        try {
            var user = authenticationHelper.tryGetUser(headers);
            return parcelService.filterByWeight(user, min, max);
        }  catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<Parcels> filterParcels(@RequestHeader HttpHeaders headers,
                                  @RequestParam(name = "customer", required = false) Optional<Integer> customerId,
                                  @RequestParam(name = "warehouse", required = false) Optional<Integer> warehouseId,
                                  @RequestParam(name = "category", required = false) Optional<Integer> categoryId) {

        if (customerId.isEmpty() && warehouseId.isEmpty() && categoryId.isEmpty()){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Parameters required.");
        }

        try {
            var user = authenticationHelper.tryGetUser(headers);
            return parcelService.filterParcels(user, customerId, warehouseId, categoryId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @GetMapping("/customer/{id}")
    public List<Parcels> getCustomerOwnParcels(@RequestHeader HttpHeaders headers,
                                               @PathVariable int id) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            return parcelService.getCustomerOwnParcels(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    //todo sort could be optimized too?

    @GetMapping("/sort/weight")
    public List<Parcels> sortByWeight(@RequestHeader HttpHeaders headers) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            return parcelService.sortByWeight(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }

    @GetMapping("/sort/date")
    public List<Parcels> sortByDate(@RequestHeader HttpHeaders headers) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            return parcelService.sortByDate(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }

    @GetMapping("/sort/weight+date")
    public List<Parcels> sortByWeightAndDate(@RequestHeader HttpHeaders headers) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            return parcelService.sortByWeightAndDate(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }
}
