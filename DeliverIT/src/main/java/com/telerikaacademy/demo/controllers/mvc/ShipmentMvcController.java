package com.telerikaacademy.demo.controllers.mvc;

import com.telerikaacademy.demo.controllers.AuthenticationHelper;
import com.telerikaacademy.demo.exceptions.DuplicateEntityException;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.OrderOfOperationsException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.Shipment;
import com.telerikaacademy.demo.models.ShipmentDto;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.services.ShipmentMapper;
import com.telerikaacademy.demo.services.ShipmentService;
import com.telerikaacademy.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Controller
@RequestMapping("/shipments")
public class ShipmentMvcController {

    private final ShipmentService shipmentService;
    private final ShipmentMapper shipmentMapper;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ShipmentMvcController(ShipmentService shipmentService, ShipmentMapper shipmentMapper, UserService userService, AuthenticationHelper authenticationHelper) {
        this.shipmentService = shipmentService;
        this.shipmentMapper = shipmentMapper;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showShipmentsPage(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            Optional<String> warehouseID = Optional.empty();
            Optional<Integer> shipmentID = Optional.empty();
            model.addAttribute("shipments",
                    shipmentService.getAll(currentUser,
                            warehouseID,
                            shipmentID));
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }
        return "shipments";
    }


    @PostMapping(consumes = "application/x-www-form-urlencoded")
    public Shipment createShipmentTest(@RequestHeader HttpHeaders headers,
                                       @Valid ShipmentDto shipmentDto) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            int employeeId = authenticationHelper.getUserIdFromAuthorizationHeader(headers);
            Shipment shipment = shipmentMapper.fromDto(shipmentDto, employeeId);
            shipmentService.create(shipment, user);
            return shipment;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    // TODO this is important to ask in the future. This method below updates the database correctly but then the data
    // base sends the old data. It has something to do with DAO, transactional, rollbacks, hibernate cache, etc.
    // UPDATE: By adding @Cacheable(value = false) to shipment model this error is fixed. Require more information
    // on why is that and what is the proper way to do it. How does cache in spring works?
     @PutMapping()
    public void update(@RequestHeader HttpHeaders headers,
                         @Valid ShipmentMvcDto shipmentMvcDto) throws ParseException {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            int employeeId = authenticationHelper.getUserIdFromAuthorizationHeader(headers);
            ShipmentDto shipmentDto = new ShipmentDto();
            Shipment shipmentIfSomethingIsEmpty =
                    shipmentService.getByID(Integer.parseInt(shipmentMvcDto.getPrimary_key()));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date arrivalDate = null;
            Date departureDate = null;
            int placeOfDeparture = 0;
            if (shipmentMvcDto.getArrival_date().isEmpty()){
                arrivalDate = shipmentIfSomethingIsEmpty.getArrival_date();
                shipmentDto.setArrival_date(arrivalDate);
            } else {
                arrivalDate = sdf.parse(shipmentMvcDto.getArrival_date());
                shipmentDto.setArrival_date(arrivalDate);
            }
            if (shipmentMvcDto.getDeparture_date().isEmpty()) {
                departureDate = shipmentIfSomethingIsEmpty.getDeparture_date();
                shipmentDto.setDeparture_date(departureDate);
            } else {
                departureDate = sdf.parse(shipmentMvcDto.getDeparture_date());
                shipmentDto.setDeparture_date(departureDate);
            }
            if (shipmentMvcDto.getPlace_of_departure().isEmpty()){
                placeOfDeparture = shipmentIfSomethingIsEmpty.getPlace_of_departure().getPrimary_key();
                shipmentDto.setPlace_of_departure(placeOfDeparture);
            } else {
                placeOfDeparture = Integer.parseInt(shipmentMvcDto.getPlace_of_departure());
                shipmentDto.setPlace_of_departure(placeOfDeparture);
            }
            shipmentDto.setPrimary_key(Integer.parseInt(shipmentMvcDto.getPrimary_key()));
            int id = Integer.parseInt(shipmentMvcDto.getPrimary_key());
            Shipment shipment = shipmentMapper.fromDto(shipmentDto, employeeId, id);
            shipmentService.update(shipment, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (DuplicateEntityException | IllegalArgumentException | OrderOfOperationsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
     }

    @DeleteMapping()
    public void delete(@RequestHeader HttpHeaders headers, ShipmentMvcDto shipmentMvcDto){
        try {
            var user = authenticationHelper.tryGetUser(headers);
            int id = Integer.parseInt(shipmentMvcDto.getPrimary_key());
            shipmentService.delete(user, id);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (OrderOfOperationsException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }
}
