package com.telerikaacademy.demo.controllers.mvc;


import com.telerikaacademy.demo.controllers.AuthenticationHelper;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.services.AddressService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/addresses")
public class AddressMvcController {
    private final AddressService addressService;
    private final AuthenticationHelper authenticationHelper;

    public AddressMvcController(AddressService addressService, AuthenticationHelper authenticationHelper) {
        this.addressService = addressService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String getAllAddresses(Model model, HttpSession session){
        User currentUser;

        try{
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!currentUser.isEmployee()){
                return "notFound";
            }
        } catch (UnauthorizedOperationException e){
            return "notFound";
        }

        model.addAttribute("addresses", addressService.getAll());
        return "addresses";
    }
}
