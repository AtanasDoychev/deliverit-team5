package com.telerikaacademy.demo.controllers.mvc;


import com.telerikaacademy.demo.controllers.AuthenticationHelper;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.Parcels;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.services.UserMapper;
import com.telerikaacademy.demo.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    public UserMvcController(UserService userService, AuthenticationHelper authenticationHelper,
                             UserMapper userMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @ModelAttribute("users")
    public List<User> populateUsers() {
        //todo temp user, to be removed
        User employee = userService.getUserByEmail("a.d@gmail.com");
        return userService.getAllUsers(employee);
    }

    @GetMapping
    public String getAllUsers(Model model, HttpSession session){
        User currentUser;

        try{
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!currentUser.isEmployee()){
                return "notFound";
            }
        } catch (UnauthorizedOperationException e){
            return "notFound";
        }

        model.addAttribute("users", userService.getAllUsers(currentUser));
        return "users";
    }
}