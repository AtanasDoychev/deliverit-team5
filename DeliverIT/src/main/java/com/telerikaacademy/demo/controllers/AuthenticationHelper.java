package com.telerikaacademy.demo.controllers;

import com.telerikaacademy.demo.exceptions.AuthenticationFailureException;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.services.UserRolesService;
import com.telerikaacademy.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AuthenticationHelper {
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    private final UserService userService;
    private final UserRolesService userRolesService;

    @Autowired
    public AuthenticationHelper(UserService userService, UserRolesService userRolesService) {
        this.userService = userService;
        this.userRolesService = userRolesService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "The requested resource requires authorization. Please enter email.");
        }
        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.getUserByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid username.");
        }
    }

    public int getUserIdFromAuthorizationHeader(HttpHeaders headers) {
        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            User user = userService.getUserByEmail(email);
            return user.getId();
        } catch (EntityNotFoundException e) {
            throw e;
        }
    }

    //this one is for the http/site version
    public User verifyAuthorization(HttpSession session, String role) {
        User user = tryGetUser(session);

        Set<String> userRoles = user
                .getRoles()
                .stream()
                .map(r -> r.getRole().toLowerCase())
                .collect(Collectors.toSet());

        if (!userRoles.contains(role.toLowerCase())) {
            throw new UnauthorizedOperationException("Users does not have the required authorization.");
        }

        return user;
    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = userService.getByUsername(username);

            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException("Wrong username/password.");
            }

            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException("Wrong username/password.");
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUserUsername = (String) session.getAttribute("currentUserUsername");

        if (currentUserUsername == null) {
            throw new UnauthorizedOperationException("No logged in user.");
        }

        try {
            return userService.getByUsername(currentUserUsername);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException("No logged in user.");
        }
    }
}
