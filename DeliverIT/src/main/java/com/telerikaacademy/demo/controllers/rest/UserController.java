package com.telerikaacademy.demo.controllers.rest;

import com.telerikaacademy.demo.controllers.AuthenticationHelper;
import com.telerikaacademy.demo.exceptions.DuplicateEntityException;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.OrderOfOperationsException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.models.UserDto;
import com.telerikaacademy.demo.models.UserRolesHelper;
import com.telerikaacademy.demo.services.UserMapper;
import com.telerikaacademy.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    @Autowired
    public UserController(UserService userService, AuthenticationHelper authenticationHelper, UserMapper userMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @PostMapping
    public UserDto createUser(@Valid @RequestBody UserDto userDto) {
        try {
            User user = userMapper.fromDto(userDto);
            userService.createUser(user);
            return userDto;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping
    public String getAllCustomersCount() {
        try {
            return userService.getAllCustomersCount();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }

    @GetMapping("/all")
    public List<User> getAllUsers(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getAllUsers(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }

    @GetMapping("/customers")
    public List<User> getAllCustomers(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getAllCustomers(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/employees")
    public List<UserRolesHelper> getAllEmployees(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getAllEmployees(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User updateUser(@RequestHeader HttpHeaders headers,
                           @PathVariable int id,
                           @Valid @RequestBody UserDto userDto) {
        try {
            User userThatUpdates = authenticationHelper.tryGetUser(headers);
            int userId = authenticationHelper.getUserIdFromAuthorizationHeader(headers);
            User userFromDto = userMapper.fromDto(userDto, id);
            return userService.updateUser(userFromDto, userId, userThatUpdates);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            userService.delete(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }

    @GetMapping("/search")
    public List<User> searchUsers(@RequestHeader HttpHeaders headers,
                                  @RequestParam(name = "email", required = false) Optional<String> email,
                                  @RequestParam(name = "firstname", required = false) Optional<String> firstname,
                                  @RequestParam(name = "lastname", required = false) Optional<String> lastname) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            return userService.searchUsers(email, firstname, lastname, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (OrderOfOperationsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
