package com.telerikaacademy.demo.controllers.mvc;

import com.telerikaacademy.demo.controllers.AuthenticationHelper;
import com.telerikaacademy.demo.exceptions.DuplicateEntityException;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.models.Warehouse;
import com.telerikaacademy.demo.models.WarehouseDto;
import com.telerikaacademy.demo.services.WarehouseMapper;
import com.telerikaacademy.demo.services.WarehouseService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;


@Controller
@RequestMapping("/warehouses")
public class WarehouseMvcController {
    private final WarehouseService warehouseService;
    private final WarehouseMapper warehouseMapper;
    private final AuthenticationHelper authenticationHelper;

    public WarehouseMvcController(WarehouseService warehouseService,
                                  WarehouseMapper warehouseMapper,
                                  AuthenticationHelper authenticationHelper) {
        this.warehouseService = warehouseService;
        this.warehouseMapper = warehouseMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showWarehousePage(Model model, HttpSession session){
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("shipments",
                    warehouseService.getAll());
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }
        return "warehouses";
    }

    @PostMapping(consumes = "application/x-www-form-urlencoded")
    public Warehouse create(@RequestHeader HttpHeaders headers,
                            @Valid WarehouseDto warehouseDto) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            Warehouse warehouse = warehouseMapper.fromDto(warehouseDto);
            warehouseService.create(user, warehouse);
            return warehouse;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
