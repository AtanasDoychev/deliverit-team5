package com.telerikaacademy.demo.controllers.rest;

import com.telerikaacademy.demo.controllers.AuthenticationHelper;
import com.telerikaacademy.demo.exceptions.DuplicateEntityException;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.Warehouse;
import com.telerikaacademy.demo.models.WarehouseDto;
import com.telerikaacademy.demo.services.WarehouseMapper;
import com.telerikaacademy.demo.services.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/warehouses")
public class WarehouseController {

    private final WarehouseService warehouseService;
    private final WarehouseMapper warehouseMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WarehouseController(WarehouseService warehouseService,
                               WarehouseMapper warehouseMapper, AuthenticationHelper authenticationHelper) {
        this.warehouseService = warehouseService;
        this.warehouseMapper = warehouseMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Warehouse> getAllWarehouses() {
        try {
            return warehouseService.getAll();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Warehouse getById(@RequestHeader HttpHeaders headers,
                             @PathVariable int id) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            return warehouseService.getById(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public Warehouse create(@RequestHeader HttpHeaders headers,
                            @Valid @RequestBody WarehouseDto warehouseDto) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            Warehouse warehouse = warehouseMapper.fromDto(warehouseDto);
            warehouseService.create(user, warehouse);
            return warehouse;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Warehouse update(@RequestHeader HttpHeaders headers,
                            @PathVariable int id,
                            @Valid @RequestBody WarehouseDto warehouseDto) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            Warehouse warehouse = warehouseMapper.fromDto(warehouseDto, id);
            warehouseService.update(user, warehouse);
            return warehouse;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            warehouseService.delete(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
