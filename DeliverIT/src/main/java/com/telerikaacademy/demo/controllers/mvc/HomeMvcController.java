package com.telerikaacademy.demo.controllers.mvc;

import com.telerikaacademy.demo.controllers.AuthenticationHelper;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.services.ParcelService;
import com.telerikaacademy.demo.services.ShipmentService;
import com.telerikaacademy.demo.services.UserService;
import com.telerikaacademy.demo.services.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final UserService userService;
    private final WarehouseService warehouseService;
    private final ParcelService parcelService;
    private final ShipmentService shipmentService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public HomeMvcController(UserService userService,
                             WarehouseService warehouseService,
                             ParcelService parcelService,
                             ShipmentService shipmentService,
                             AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.warehouseService = warehouseService;
        this.parcelService = parcelService;
        this.shipmentService = shipmentService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showHomePage(Model model, HttpSession session) {
        model.addAttribute("allWarehouses", warehouseService.getAll());
        model.addAttribute("allCustomersCount", userService.getAllCustomersCount());

        //These under to the 'TRY' are for showing parcels, customers and addresses
        User employee = userService.getUserByEmail("a.d@gmail.com");
        model.addAttribute("parcelsCount",
                String.valueOf(parcelService.getAll(employee).size()));
        //todo this must be changed to get all customers, but at the moment there is problem
        //when somebody is registering the role given is employee instead of customer
        model.addAttribute("userCount",
                String.valueOf(userService.getAllUsers(employee).size()));
        model.addAttribute("allWarehouses", warehouseService.getAll());

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }

        return "index";
    }

}
