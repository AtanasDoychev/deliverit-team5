package com.telerikaacademy.demo.controllers.rest;


import com.telerikaacademy.demo.controllers.AuthenticationHelper;
import com.telerikaacademy.demo.exceptions.DuplicateEntityException;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.OrderOfOperationsException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.Shipment;
import com.telerikaacademy.demo.models.ShipmentDto;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.services.ShipmentMapper;
import com.telerikaacademy.demo.services.ShipmentService;
import com.telerikaacademy.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/shipments")
public class ShipmentController {

    private final ShipmentService shipmentService;
    private final ShipmentMapper shipmentMapper;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;

    @Autowired
    public ShipmentController(ShipmentService shipmentService, ShipmentMapper shipmentMapper,
                              AuthenticationHelper authenticationHelper, UserService userService) {
        this.shipmentService = shipmentService;
        this.shipmentMapper = shipmentMapper;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
    }

    @GetMapping
    public List<Shipment> getAllShipments(@RequestHeader HttpHeaders headers,
                                          @RequestParam(name = "warehouseID",required = false)
                                                  Optional<String> warehouseID,
                                          @RequestParam(name = "id", required = false)
                                                Optional<Integer> shipmentID) {
        try {
              var user = authenticationHelper.tryGetUser(headers);
          //  User user = userService.getUserByEmail("a.d@gmail.com");
            return shipmentService.getAll(user, warehouseID, shipmentID);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException | OrderOfOperationsException e){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }

    @GetMapping("/{status}")
    public List<Shipment> getAllShipmentsWithStatus(@RequestHeader HttpHeaders headers,
                                                    @PathVariable String status){
        try {
            var user = authenticationHelper.tryGetUser(headers);
            return shipmentService.getAllWithStatus(user, status);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }

    @GetMapping("/nextarrival")
    public Shipment getNextArrivingShipment(@RequestHeader HttpHeaders headers){
        try {
            var user = authenticationHelper.tryGetUser(headers);
            return shipmentService.getNextArrivingShipment(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }



    @GetMapping("/myshipment")
    public Shipment getMyShipments(@RequestHeader HttpHeaders headers,
                                         @RequestParam int parcelID) {
        var user = authenticationHelper.tryGetUser(headers);
        return shipmentService.getCustomerShipments(user, parcelID);
    }

    @PostMapping
    public Shipment createShipment(@RequestHeader HttpHeaders headers,
                                   @Valid @RequestBody ShipmentDto shipmentDto) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            int employeeId = authenticationHelper.getUserIdFromAuthorizationHeader(headers);
            Shipment shipment = shipmentMapper.fromDto(shipmentDto, employeeId);
            shipmentService.create(shipment, user);
            return shipment;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @PutMapping("/{id}")
    public Shipment update(@RequestHeader HttpHeaders headers,
                           @PathVariable int id,
                           @Valid @RequestBody ShipmentDto shipmentDto) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            int employeeId = authenticationHelper.getUserIdFromAuthorizationHeader(headers);
            Shipment shipment = shipmentMapper.fromDto(shipmentDto, employeeId, id);
            shipmentService.update(shipment, user);
            return shipment;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (DuplicateEntityException | IllegalArgumentException | OrderOfOperationsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id){
        try {
            var user = authenticationHelper.tryGetUser(headers);
            shipmentService.delete(user, id);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        } catch (OrderOfOperationsException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }
}
