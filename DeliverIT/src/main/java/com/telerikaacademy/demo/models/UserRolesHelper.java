package com.telerikaacademy.demo.models;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;

@Entity
@Table(name = "user_roles")
public class UserRolesHelper {

    @Id
    @Column(name = "primary_key")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Autowired
    public UserRolesHelper() {
    }

    @OneToOne
    @JoinColumn (name = "user_id")
    private User userId;

    @ManyToOne(targetEntity = Role.class)
    @JoinColumn (name = "role_id")
    private Role roleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Role getRoleId() {
        return roleId;
    }

    public void setRoleId(Role roleId) {
        this.roleId = roleId;
    }
}
