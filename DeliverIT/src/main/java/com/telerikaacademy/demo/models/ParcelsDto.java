package com.telerikaacademy.demo.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class ParcelsDto {

    @NotNull(message = "Warehouse ID cannot be empty.")
    @Positive(message = "Warehouse ID should be positive.")
    private int warehouseID;

    @NotNull(message = "Weight cannot be empty.")
    @Positive(message = "Weight should be positive.")
    private double weight;

    @NotNull(message = "Category ID cannot be empty.")
    @Positive(message = "Category ID should be positive.")
    private int categoryID;

    @NotNull(message = "Customer ID cannot be empty.")
    @Positive(message = "Customer ID should be positive.")
    private int customerID;

    @NotNull(message = "Shipment ID cannot be empty.")
    @Positive(message = "Shipment ID should be positive.")
    private int shipmentID;


    public ParcelsDto() {
    }

    public int getWarehouseID() {
        return warehouseID;
    }

    public void setWarehouseID(int warehouseID) {
        this.warehouseID = warehouseID;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getShipmentID() {
        return shipmentID;
    }

    public void setShipmentID(int shipmentID) {
        this.shipmentID = shipmentID;
    }


}
