package com.telerikaacademy.demo.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Set;


@Entity
@Cacheable(value = false)
@Table(name = "shipments")
public class Shipment {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "primary_key")
    private int id;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "arrival_date")
    private Date arrival_date;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "departure_date")
    private Date departure_date;

    @OneToOne
    @JoinColumn(name = "place_of_departure")
    private Warehouse place_of_departure;

    @Column(name = "employee_id")
    private int employee_id;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_parcels",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "parcel_id")
    )
    private Set<Parcels> listOfParcels;

    @Transient
    private Statuses status;

    public Shipment() {
    }

    public int getId() {
        return this.id;
    }

    public Date getArrival_date() {
        return arrival_date;
    }

    public Date getDeparture_date() {
        return departure_date;
    }

    public Warehouse getPlace_of_departure() {
        return place_of_departure;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public Statuses getStatus() {
        return determineStatus();
    }

    public void setPlace_of_departure(Warehouse place_of_departure) {
        this.place_of_departure = place_of_departure;
    }

    public void setEmployeeId(int employeeID) {
        this.employee_id = employeeID;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setArrival_date(Date arrival_date) {
        this.arrival_date = arrival_date;
    }

    @JsonIgnore
    public String getStatusAsString() {
        return String.valueOf(getStatus());
    }

    public void setDeparture_date(Date departure_date) {
        this.departure_date = departure_date;
    }

    public Set<Parcels> getListOfParcels() {
        return listOfParcels;
    }

    public void setListOfParcels(Set<Parcels> listOfParcels) {
        this.listOfParcels = listOfParcels;
    }

    public LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    private Statuses determineStatus() {
        if (getArrival_date() == null && getDeparture_date() == null) {
            return Statuses.PREPARING;
        }
        LocalDate now = LocalDate.now();
        LocalDate departure = convertToLocalDateViaInstant(getDeparture_date());
        LocalDate arrival = convertToLocalDateViaInstant(getArrival_date());
        if (now.isBefore(departure)) {
            return Statuses.PREPARING;
        }
        if (now.isAfter(departure) && now.isBefore(arrival)) {
            return Statuses.ONTHEWAY;
        }
        if (now.isAfter(arrival)) {
            return Statuses.COMPLETED;
        }
        return null;
    }
}
