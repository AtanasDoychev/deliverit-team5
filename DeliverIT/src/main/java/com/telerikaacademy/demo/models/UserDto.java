package com.telerikaacademy.demo.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

//for postman
public class UserDto {

    @NotNull
    @Size(min = 2, max = 15, message = "Name should be between 2 and 20 symbols")
    private String first_name;

    @NotNull
    @Size(min = 2, max = 15, message = "Name should be between 2 and 20 symbols")
    private String last_name;

    @NotNull
    @Size(min = 2, max = 30, message = "Name should be between 2 and 20 symbols")
    private String email;

    @NotNull
    @Positive
    private int address;

    public UserDto() {
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }
}
