package com.telerikaacademy.demo.models;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UserRegisterDto {

    @NotEmpty
    @Size(min = 2, max = 25)
    private String username;

    @NotEmpty
    @Size(min = 2, max = 25)
    private String password;

    @NotEmpty
    @Size(min = 2, max = 25)
    private String repeatPassword;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Size(min = 2, max = 25)
    private String firstName;

    @NotEmpty
    @Size(min = 2, max = 25)
    private String lastName;

    public UserRegisterDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
