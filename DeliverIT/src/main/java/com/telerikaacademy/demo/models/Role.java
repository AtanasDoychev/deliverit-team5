package com.telerikaacademy.demo.models;

import javax.persistence.*;

@Entity
@Table(name = "roles")
public class Role {


    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private int id;

    @Column(name = "type")
    private String role;

    public Role() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() { return role; }

    public void setRole(String role) {
        this.role = role;
    }

}
