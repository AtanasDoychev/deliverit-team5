package com.telerikaacademy.demo.models;

public enum Statuses {
    PREPARING, ONTHEWAY, COMPLETED;
}
