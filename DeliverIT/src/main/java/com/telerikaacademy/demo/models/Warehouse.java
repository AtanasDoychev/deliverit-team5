package com.telerikaacademy.demo.models;

import javax.persistence.*;

@Entity
@Table(name = "warehouses")
public class Warehouse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "primary_key")
    private int primary_key;

    @OneToOne
    @JoinColumn(name = "address_id")
    private Address address;

    public Warehouse() {
    }


    public Warehouse(int primary_key, Address address) {
        this.primary_key = primary_key;
        this.address = address;
    }

    public int getPrimary_key() {
        return primary_key;
    }

    public void setPrimary_key(int primary_key) {
        this.primary_key = primary_key;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
