package com.telerikaacademy.demo.models;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;

@Entity
@Table(name = "categories")
public class Categories {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "primary_key")
    private int id;

    @Column(name = "category")
    private String category;


    @Autowired
    public Categories(String category) {
        this.category = category;
    }

    public Categories() {
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
