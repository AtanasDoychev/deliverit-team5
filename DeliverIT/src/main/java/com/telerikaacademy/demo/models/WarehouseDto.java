package com.telerikaacademy.demo.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class WarehouseDto {

    @NotNull(message = "Address cannot be empty.")
    @Positive(message = "ID must be positive number.")
    private int address_id;


    public WarehouseDto() {
    }

    public int getAddress_id() {
        return address_id;
    }

    public void setAddress_id(int address_id) {
        this.address_id = address_id;
    }
}
