package com.telerikaacademy.demo.models;

import javax.persistence.Cacheable;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Date;

@Cacheable(value = false)
public class ShipmentDto {

    public ShipmentDto() {
    }

    private int primary_key;

    @NotNull
    @Positive
    private int place_of_departure;

    @FutureOrPresent
    private Date departure_date;

    @FutureOrPresent
    private Date arrival_date;

    public int getPlace_of_departure() {
        return place_of_departure;
    }

    public void setPlace_of_departure(int place_of_departure) {
        this.place_of_departure = place_of_departure;
    }

    public Date getDeparture_date() {
        return departure_date;
    }

    public Date getArrival_date() {
        return arrival_date;
    }

    public int getPrimary_key() {
        return primary_key;
    }

    public void setPrimary_key(int primary_key) {
        this.primary_key = primary_key;
    }

    public void setDeparture_date(Date departure_date) {
        this.departure_date = departure_date;
    }

    public void setArrival_date(Date arrival_date) {
        this.arrival_date = arrival_date;
    }


}
