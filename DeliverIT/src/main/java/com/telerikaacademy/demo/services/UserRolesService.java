package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.Role;

public interface UserRolesService {

    Role checkIfEmployeeOrCustomer(int id);
}
