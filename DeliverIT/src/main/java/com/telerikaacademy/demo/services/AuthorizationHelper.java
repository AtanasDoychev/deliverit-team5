package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.User;
import org.springframework.stereotype.Component;

@Component
public class AuthorizationHelper {


    public static void verifyIfEmployee(User user, String message) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(message);
        }
    }

    public static void verifyIfCustomer(User user, String message) {
        if (!user.isCustomer()) {
            throw new UnauthorizedOperationException(message);
        }
    }
}
