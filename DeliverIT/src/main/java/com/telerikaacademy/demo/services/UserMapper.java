package com.telerikaacademy.demo.services;


import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.models.UserDto;
import com.telerikaacademy.demo.models.UserRegisterDto;
import com.telerikaacademy.demo.repository.AddressRepositoryImpl;
import com.telerikaacademy.demo.repository.UserRepository;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class UserMapper {

    private final AddressRepositoryImpl addressRepository;
    private final UserRepository userRepository;

    public UserMapper(AddressRepositoryImpl addressRepository, UserRepository userRepository) {
        this.addressRepository = addressRepository;
        this.userRepository = userRepository;
    }

    public User fromDto(UserDto userDto, int id) {
        User user = userRepository.getUserByID(id);
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    private void dtoToObject(UserDto userDto, User user) {
        user.setFirstName(userDto.getFirst_name());
        user.setLastName(userDto.getLast_name());
        user.setEmail(userDto.getEmail());
        user.setAddress(addressRepository.getById(userDto.getAddress()));
    }

    public User fromDto(UserRegisterDto registerDto, User user) {
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setEmail(registerDto.getEmail());
        return user;
    }

}
