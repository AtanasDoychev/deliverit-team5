package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.exceptions.DuplicateEntityException;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.models.Warehouse;
import com.telerikaacademy.demo.repository.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikaacademy.demo.services.AuthorizationHelper.verifyIfEmployee;

@Service
public class WarehouseServiceImpl implements WarehouseService{

    public static final String MODIFY_WAREHOUSE_ERROR_MESSAGE =
            "Only employees can modify or create a parcel.";

    private final WarehouseRepository warehouseRepository;

    @Autowired
    public WarehouseServiceImpl(WarehouseRepository warehouseRepository) {
        this.warehouseRepository = warehouseRepository;
    }

    @Override
    public List<Warehouse> getAll() {
        return warehouseRepository.getAll();
    }

    @Override
    public Warehouse getById(User user, int id) {
        verifyIfEmployee(user, MODIFY_WAREHOUSE_ERROR_MESSAGE);
        return warehouseRepository.getById(id);
    }

    @Override
    public void create(User user, Warehouse warehouse) {
        boolean duplicateExists = true;
        try {
            verifyIfEmployee(user, MODIFY_WAREHOUSE_ERROR_MESSAGE);
            warehouseRepository.getByAddress(warehouse.getAddress());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Warehouse", "address_id",
                    String.valueOf(warehouse.getAddress().getId()));
        }
        warehouseRepository.create(warehouse);
    }

    @Override
    public void update(User user, Warehouse warehouse) {
        boolean duplicateExists = true;
        try {
            verifyIfEmployee(user, MODIFY_WAREHOUSE_ERROR_MESSAGE);
            Warehouse existingWarehouse = warehouseRepository.getByAddress(
                    warehouse.getAddress());
            if (existingWarehouse.getAddress().equals(warehouse.getAddress())) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (!duplicateExists) {
            throw new EntityNotFoundException("Warehouse", "address_id",
                    String.valueOf(warehouse.getAddress().getId()));
        }
        warehouseRepository.update(warehouse);
    }

    @Override
    public void delete(User user, int id) {
        verifyIfEmployee(user, MODIFY_WAREHOUSE_ERROR_MESSAGE);
        warehouseRepository.delete(id);
    }
}
