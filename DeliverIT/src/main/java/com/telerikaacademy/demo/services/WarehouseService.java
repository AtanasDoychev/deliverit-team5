package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.models.Warehouse;

import java.util.List;

public interface WarehouseService {

    List<Warehouse> getAll();

    Warehouse getById(User user, int id);

    void create(User user, Warehouse warehouse);

    void update(User user, Warehouse warehouse);

    void delete(User user, int id);
}
