package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.models.Parcels;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.repository.ParcelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikaacademy.demo.services.AuthorizationHelper.verifyIfCustomer;
import static com.telerikaacademy.demo.services.AuthorizationHelper.verifyIfEmployee;

@Service
public class ParcelServiceImpl implements ParcelService {

    public static final String EMPLOYEE_MODIFY_PARCEL_ERROR_MESSAGE =
            "Only employees can modify or create a parcel.";
    public static final String CUSTOMER_MODIFY_PARCEL_ERROR_MESSAGE =
            "Only customer can modify its own parcel.";

    private final ParcelRepository parcelRepository;

    @Autowired
    public ParcelServiceImpl(ParcelRepository parcelRepository) {
        this.parcelRepository = parcelRepository;
    }

    @Override
    public List<Parcels> getAll(User user) {
        verifyIfEmployee(user, EMPLOYEE_MODIFY_PARCEL_ERROR_MESSAGE);
        return parcelRepository.getAll();
    }

    @Override
    public Parcels getById(int id, User user) {
        verifyIfEmployee(user, EMPLOYEE_MODIFY_PARCEL_ERROR_MESSAGE);
        return parcelRepository.getById(id);
    }

    @Override
    public void create(Parcels parcels, User user) {
        verifyIfEmployee(user, EMPLOYEE_MODIFY_PARCEL_ERROR_MESSAGE);
        parcelRepository.create(parcels);
    }

    @Override
    public void update(Parcels parcels, User user) {
        verifyIfEmployee(user, EMPLOYEE_MODIFY_PARCEL_ERROR_MESSAGE);
        Parcels existingParcel = parcelRepository.getById(parcels.getId());
        if (existingParcel.getId() == parcels.getId()) {
            parcelRepository.update(parcels);
        }
    }

    @Override
    public void delete(User user, int id) {
        verifyIfEmployee(user, EMPLOYEE_MODIFY_PARCEL_ERROR_MESSAGE);
        Parcels parcels = parcelRepository.getById(id);
        parcelRepository.delete(parcels);
    }

    @Override
    public List<Parcels> filterByWeight(User user, int min, int max) {
        verifyIfEmployee(user, EMPLOYEE_MODIFY_PARCEL_ERROR_MESSAGE);
        return parcelRepository.filterByWeight(min, max);
    }

    @Override
    public List<Parcels> filterParcels(User user, Optional<Integer> customerId,
                                       Optional<Integer> warehouseId,
                                       Optional<Integer> categoryId) {

        verifyIfEmployee(user, EMPLOYEE_MODIFY_PARCEL_ERROR_MESSAGE);

        List<Parcels> temp = new ArrayList<>();

        if (customerId.isPresent()){
            List<Parcels> tempCu = parcelRepository.filterByCustomer(customerId);
            if (!tempCu.isEmpty()){
                temp.addAll(tempCu);
            }
        }

        if (warehouseId.isPresent()){
            List<Parcels> tempW = parcelRepository.filterByWarehouse(warehouseId);
            if (!tempW.isEmpty()){
                temp.addAll(tempW);
            }
        }

        if (categoryId.isPresent()){
            List<Parcels> tempPr = parcelRepository.filterByCategory(categoryId);
            if (!tempPr.isEmpty()){
                temp.addAll(tempPr);
            }
        }

        if (temp.isEmpty()){
            throw new EntityNotFoundException("No parcels found.");
        }
        return temp;
    }

    @Override
    public List<Parcels> getCustomerOwnParcels(User user, int customerId) {
        verifyIfCustomer(user, CUSTOMER_MODIFY_PARCEL_ERROR_MESSAGE);
        return parcelRepository.getCustomerOwnParcels(customerId);
    }

    @Override
    public List<Parcels> sortByWeight(User user) {
        verifyIfEmployee(user, EMPLOYEE_MODIFY_PARCEL_ERROR_MESSAGE);
        return parcelRepository.sortByWeight();
    }

    @Override
    public List<Parcels> sortByDate(User user) {
        verifyIfEmployee(user, EMPLOYEE_MODIFY_PARCEL_ERROR_MESSAGE);
        return parcelRepository.sortByDate();
    }

    @Override
    public List<Parcels> sortByWeightAndDate(User user) {
        verifyIfEmployee(user, EMPLOYEE_MODIFY_PARCEL_ERROR_MESSAGE);
        return parcelRepository.sortByWeightAndDate();
    }
}

