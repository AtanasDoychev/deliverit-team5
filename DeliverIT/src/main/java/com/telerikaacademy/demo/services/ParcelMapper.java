package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.*;
import com.telerikaacademy.demo.repository.*;
import org.springframework.stereotype.Component;

@Component
public class ParcelMapper {

    private final ParcelRepository parcelRepository;
    private final WarehouseRepository warehouseRepository;
    private final CategoriesRepository categoriesRepository;
    private final UserRepository userRepository;
    private final ShipmentRepository shipmentRepository;

    public ParcelMapper(ParcelRepository parcelRepository,
                        WarehouseRepository warehouseRepository,
                        CategoriesRepository categoriesRepository,
                        UserRepository userRepository,
                        ShipmentRepository shipmentRepository) {
        this.parcelRepository = parcelRepository;
        this.warehouseRepository = warehouseRepository;
        this.categoriesRepository = categoriesRepository;
        this.userRepository = userRepository;
        this.shipmentRepository = shipmentRepository;
    }

    //when creating
    public Parcels fromDto(ParcelsDto parcelsDto, int employeeID) {
        Parcels parcels = new Parcels();
        dtoToObject(parcelsDto, parcels, employeeID);
        return parcels;
    }

    //when updating
    public Parcels fromDto(ParcelsDto parcelsDto, int employeeId, int id) {
        Parcels parcels = new Parcels();
        dtoToObject(parcelsDto, parcels, employeeId, id);
        return parcels;
    }

    //when create
    private void dtoToObject(ParcelsDto parcelsDto, Parcels parcels, int employeeID) {

        Warehouse warehouse = warehouseRepository.getById(parcelsDto.getWarehouseID());
        Categories category = categoriesRepository.getById(parcelsDto.getCategoryID());
        User customer = userRepository.getUserByID(parcelsDto.getCustomerID());
        User employee = userRepository.getUserByID(employeeID);
        Shipment shipment = shipmentRepository.getById(parcelsDto.getShipmentID());

        parcels.setWarehouse(warehouse);
        parcels.setWeight(parcelsDto.getWeight());
        parcels.setCategory(category);
        parcels.setCustomer(customer);
        parcels.setEmployee(employee);
        parcels.setShipment(shipment);
    }

    //when update
    private void dtoToObject(ParcelsDto parcelsDto, Parcels parcels, int employeeID, int id) {

        Warehouse warehouse = warehouseRepository.getById(parcelsDto.getWarehouseID());
        Categories category = categoriesRepository.getById(parcelsDto.getCategoryID());
        Shipment shipment = shipmentRepository.getById(parcelsDto.getShipmentID());
        User customer = userRepository.getUserByID(parcelsDto.getCustomerID());
        User employee = userRepository.getUserByID(employeeID);

        parcels.setId(id);
        parcels.setWarehouse(warehouse);
        parcels.setWeight(parcelsDto.getWeight());
        parcels.setCategory(category);
        parcels.setCustomer(customer);
        parcels.setEmployee(employee);
        parcels.setShipment(shipment);
    }

    public ParcelsDto toDto(Parcels parcels) {
        ParcelsDto parcelsDto = new ParcelsDto();
        parcelsDto.setWarehouseID(parcels.getWarehouse().getPrimary_key());
        parcelsDto.setWeight(parcels.getWeight());
        parcelsDto.setCategoryID(parcels.getCategory().getId());
        parcelsDto.setCustomerID(parcels.getCustomer().getId());
        parcelsDto.setShipmentID(parcels.getShipment().getId());
        return parcelsDto;
    }
}
