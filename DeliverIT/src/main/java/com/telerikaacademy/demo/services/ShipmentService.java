package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.Shipment;
import com.telerikaacademy.demo.models.User;

import java.util.List;
import java.util.Optional;

public interface ShipmentService {
    List<Shipment> getAll(User user, Optional<String> warehouseID, Optional<Integer> shipmentID);

    Shipment getCustomerShipments(User user, int parcelID);

    void create(Shipment shipment, User user);

    void update(Shipment shipment, User user);

    void delete(User user, int id);

    List<Shipment> getAllWithStatus(User user, String status);

    List<Shipment> getAllFilteredByWarehouse(User user, int warehouseID);

    Shipment getNextArrivingShipment(User user);

    void verifyIfEmployee(User user);

    void verifyIfCustomer(User user);

    Shipment getByID(int id);
}
