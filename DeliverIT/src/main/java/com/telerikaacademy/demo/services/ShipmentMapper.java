package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.*;
import com.telerikaacademy.demo.repository.ShipmentRepository;
import com.telerikaacademy.demo.repository.WarehouseRepository;
import org.springframework.stereotype.Component;

@Component
public class ShipmentMapper {

    private final WarehouseRepository warehouseRepository;

    public ShipmentMapper(WarehouseRepository warehouseRepository) {
        this.warehouseRepository = warehouseRepository;
    }

    //when update
    public Shipment fromDto(ShipmentDto shipmentDto, int employeeId, int id) {
        Shipment shipment = new Shipment();
        dtoToObject(shipmentDto, shipment, employeeId, id);
        return shipment;
    }

    //when create
    public Shipment fromDto(ShipmentDto shipmentDto, int employeeID) {
        Shipment shipment = new Shipment();
        dtoToObject(shipmentDto, shipment, employeeID);
        return shipment;
    }

    //when update
    private void dtoToObject(ShipmentDto shipmentDto, Shipment shipment, int employeeId, int id) {
        Warehouse warehouse = warehouseRepository.getById(shipmentDto.getPlace_of_departure());
        shipment.setPlace_of_departure(warehouse);
        shipment.setEmployeeId(employeeId);
        shipment.setId(id);
        if (shipmentDto.getArrival_date().after(shipmentDto.getDeparture_date())) {
            shipment.setArrival_date(shipmentDto.getArrival_date());
            shipment.setDeparture_date(shipmentDto.getDeparture_date());
        } else {
            throw new IllegalArgumentException("Arrival date cannot be before departure date");
        }
    }

    //when create
    private void dtoToObject(ShipmentDto shipmentDto, Shipment shipment, int employeeID) {
       Warehouse warehouse = warehouseRepository.getById(shipmentDto.getPlace_of_departure());
       shipment.setPlace_of_departure(warehouse);
       shipment.setEmployeeId(employeeID);
    }
}
