package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.Categories;

import java.util.List;

public interface CategoryService {
    List<Categories> getAll();
}
