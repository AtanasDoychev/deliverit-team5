package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.Country;

import java.util.List;

public interface CountryService {
    List<Country> getAll();
}
