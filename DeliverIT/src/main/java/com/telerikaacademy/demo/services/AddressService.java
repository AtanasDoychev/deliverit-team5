package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.Address;

import java.util.List;

public interface AddressService {
    List<Address> getAll();
}
