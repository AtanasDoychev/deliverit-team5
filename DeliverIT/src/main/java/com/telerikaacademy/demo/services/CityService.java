package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.City;

import java.util.List;

public interface CityService {
    List<City> getAll();
}
