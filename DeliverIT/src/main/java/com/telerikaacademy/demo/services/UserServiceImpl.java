package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.exceptions.DuplicateEntityException;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.OrderOfOperationsException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.Parcels;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.models.UserRolesHelper;
import com.telerikaacademy.demo.repository.UserRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.telerikaacademy.demo.services.AuthorizationHelper.verifyIfEmployee;

@Service
public class UserServiceImpl implements UserService {

    public static final String MODIFY_USER_ERROR_MESSAGE = "Only employees can modify an user.";
    private final UserRepositoryImpl userRepository;

    @Autowired
    public UserServiceImpl(UserRepositoryImpl userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public String getAllCustomersCount() {
        return userRepository.getAllCustomersCount();
    }

    @Override
    public List<User> getAllUsers(User user) {
        verifyIfEmployee(user);
        return userRepository.getAll();
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public User getById(int id, User user) {
        verifyIfEmployee(user);
        return userRepository.getUserByID(id);
    }

    @Override
    public List<User> getAllCustomers(User user) {
        try {
            verifyIfEmployee(user);
            return userRepository.getAllCustomers();
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            throw e;
        }
    }

    @Override
    public List<UserRolesHelper> getAllEmployees(User user) {
        try {
            verifyIfEmployee(user);
            return userRepository.getAllEmployees();
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            throw e;
        }
    }

    @Override
    public List<User> getAllEmployees2(User user) {
        try {
            verifyIfEmployee(user);
            return userRepository.getAllEmployees2();
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            throw e;
        }
    }

    @Override
    public void createUser(User user) {
        boolean duplicateExists = false;
        try {
            User existingUser = userRepository.getUserByEmail(user.getEmail());
            if (existingUser.getId() == user.getId()) {
                duplicateExists = true;
            }
        } catch (NullPointerException e) {
            duplicateExists = true;
        } catch (EntityNotFoundException e){
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException(user.getEmail());
        }
            userRepository.createUser(user);
        }

    @Override
    public User updateUser(User user, int userId, User userThatUpdates) {
        try {
            if (userId != user.getId()) {
                verifyIfEmployee(userThatUpdates);
            }
        } catch (UnauthorizedOperationException e){
            throw new UnauthorizedOperationException("Only employees or the owner of the account can edit.");
        }
        boolean duplicateExists = true;
        try {
            User existingUser = userRepository.getUserByEmail(user.getEmail());
            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new EntityNotFoundException("User", "id", String.valueOf(user.getId()));
        }
        userRepository.updateUser(user);
        return user;
    }

    @Override
    public void delete(User user, int id) {
        verifyIfEmployee(user);
        userRepository.delete(id);
    }

    @Override
    public List<User> searchUsers(Optional<String> email,
                                  Optional<String> firstname,
                                  Optional<String> lastname,
                                  User user) {
        verifyIfEmployee(user);
        if (email.isEmpty() && firstname.isEmpty() && lastname.isEmpty()){
            throw new OrderOfOperationsException("Search requires at least 1 param.");
        } else if (firstname.isPresent() && lastname.isEmpty() || (firstname.isEmpty() && lastname.isPresent())){
            throw new OrderOfOperationsException("Search by name needs both firstname and lastname");
        } else if (email.isPresent() && firstname.isPresent() && lastname.isPresent()){
            throw new OrderOfOperationsException("Search can be by email only or firstname and lastname only.");
        }
        if (email.isPresent()){
            return userRepository.getUserByPartialEmail(email);
        } else return userRepository.getUserByFirstNameAndLastName(firstname, lastname);
    }

    @Override
    public User getUserByEmail(String email) {
        try {
            return userRepository.getUserByEmail(email);
        } catch (EntityNotFoundException e) {
            throw e;
        }
    }


    public void verifyIfEmployee(User user) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }
    }

    public void verifyIfCustomer(User user) {
        if (!user.isCustomer()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }
    }

}
