package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.Parcels;
import com.telerikaacademy.demo.models.User;

import java.util.List;
import java.util.Optional;

public interface ParcelService {

    List<Parcels> getAll(User user);

    Parcels getById(int id, User user);

    void create(Parcels parcels, User user);

    void update(Parcels parcels, User user);

    void delete(User user, int id);

    List<Parcels> filterByWeight(User user, int min, int max);

    List<Parcels> getCustomerOwnParcels(User user, int customerId);

    List<Parcels> sortByWeight(User user);

    List<Parcels> sortByDate(User user);

    List<Parcels> sortByWeightAndDate(User user);

    List<Parcels> filterParcels(User user, Optional<Integer> customerId,
                                Optional<Integer> warehouseId, Optional<Integer> categoryId);
}
