package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.models.UserRolesHelper;

import java.util.List;
import java.util.Optional;

public interface UserService {

    String getAllCustomersCount();

    List<User> getAllCustomers(User user);

    List<User> getAllEmployees2(User user);

    User getUserByEmail(String email);

    List<User> getAllUsers(User user);

    List<UserRolesHelper> getAllEmployees(User user);

    void createUser(User user);

    User updateUser(User user, int userId, User userThatUpdates);

    void delete(User user, int id);

    List<User> searchUsers(Optional<String> email, Optional<String> firstname, Optional<String> lastname, User user);

    User getByUsername(String username);

    User getById(int id, User user);
}
