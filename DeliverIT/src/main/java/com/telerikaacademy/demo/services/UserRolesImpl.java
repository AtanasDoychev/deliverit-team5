package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.models.Role;
import com.telerikaacademy.demo.repository.UserRolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRolesImpl implements UserRolesService{

    private final UserRolesRepository userRolesRepository;

    @Autowired
    public UserRolesImpl(UserRolesRepository userRolesRepository) {
        this.userRolesRepository = userRolesRepository;
    }

    @Override
    public Role checkIfEmployeeOrCustomer(int id) {
        return userRolesRepository.checkIfEmployeeOrCustomer(id);
    }
}
