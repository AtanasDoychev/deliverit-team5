package com.telerikaacademy.demo.services;

import com.telerikaacademy.demo.exceptions.DuplicateEntityException;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.OrderOfOperationsException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.Shipment;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.repository.ShipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShipmentServiceImpl implements ShipmentService {

    public static final String MODIFY_SHIPMENT_ERROR_MESSAGE = "Only employees can modify a shipment.";

    private final ShipmentRepository shipmentRepository;

    @Autowired
    public ShipmentServiceImpl(ShipmentRepository shipmentRepository) {
        this.shipmentRepository = shipmentRepository;
    }

    @Override
    public List<Shipment> getAll(User user, Optional<String> warehouseID, Optional<Integer> shipmentID) {
        verifyIfEmployee(user);
        if (warehouseID.isEmpty() && shipmentID.isEmpty()) {
            return shipmentRepository.getAll();
        } else if (warehouseID.isPresent() && shipmentID.isPresent()){
            throw new OrderOfOperationsException("You can search only by one criteria at a time.");
        } else if (warehouseID.isPresent()){
            return shipmentRepository.getAllFilteredByWarehouse(Integer.parseInt(warehouseID.get()));
        } else if (shipmentID.isPresent()){
             return shipmentRepository.getShipmentByID(shipmentID.get());
        }
        return null;
    }

    @Override
    public Shipment getCustomerShipments(User user, int parcelID) {
       if (!shipmentRepository.isUserOwnerOfParcel(user,parcelID)){
            verifyIfEmployee(user);
        }
        return shipmentRepository.getCustomerShipments(parcelID);
    }

    @Override
    public void create(Shipment shipment, User user) {
        verifyIfEmployee(user);
        shipmentRepository.create(shipment);
    }

    @Override
    public void update(Shipment shipment, User user) {
        verifyIfEmployee(user);
        boolean duplicateExists = true;
        try {
            shipmentRepository.getById(shipment.getId());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (!duplicateExists) {
            throw new EntityNotFoundException("Shipment", "id", String.valueOf(shipment.getId()));
        }
        shipmentRepository.update(shipment);
    }

    @Override
    public void delete(User user, int id) {
        verifyIfEmployee(user);
        Shipment shipment = shipmentRepository.getById(id);
        shipmentRepository.delete(shipment);
    }

    @Override
    public List<Shipment> getAllWithStatus(User user, String status) {
        verifyIfEmployee(user);
        return shipmentRepository.getAllWithStatus(status);
    }

    @Override
    public List<Shipment> getAllFilteredByWarehouse(User user, int warehouseID) {
        verifyIfEmployee(user);
        return shipmentRepository.getAllFilteredByWarehouse(warehouseID);
    }

    @Override
    public Shipment getNextArrivingShipment(User user) {
        verifyIfEmployee(user);
        return shipmentRepository.getNextArrivingShipment();
    }

    @Override
    public void verifyIfEmployee(User user) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(MODIFY_SHIPMENT_ERROR_MESSAGE);
        }
    }
    @Override
    public void verifyIfCustomer(User user) {
        if (!user.isCustomer()) {
            throw new UnauthorizedOperationException(MODIFY_SHIPMENT_ERROR_MESSAGE);
        }
    }

    @Override
    public Shipment getByID(int id) {
        return shipmentRepository.getById(id);
    }
}