package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.models.UserRolesHelper;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    String getAllCustomersCount();

    List<User> getAllCustomers();

    List<User> getAllEmployees2();

    User getUserByEmail(String email);

    User getUserByID(int id);

    List<User> getAll();

    List<UserRolesHelper> getAllEmployees();

    void createUser(User user);

    void updateUser(User user);

    void delete(int id);

    List<User> getUserByPartialEmail(Optional<String> email);

    List<User> getUserByFirstNameAndLastName(Optional<String> firstname, Optional<String> lastname);

    User getByUsername(String username);

}
