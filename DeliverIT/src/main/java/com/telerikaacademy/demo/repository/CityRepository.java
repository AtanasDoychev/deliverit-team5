package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.models.City;

import java.util.List;

public interface CityRepository {

    List<City> getAll();

    City getById(int id);
}
