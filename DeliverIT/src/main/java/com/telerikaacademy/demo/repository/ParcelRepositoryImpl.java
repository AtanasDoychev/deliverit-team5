package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.models.Parcels;
import com.telerikaacademy.demo.models.UserParcels;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Repository
public class ParcelRepositoryImpl implements ParcelRepository {

    public static final String NO_PARCELS_FOUND = "No parcels found.";
    public static final String PARCELS_BELONGING_TO_CUSTOMER = "Parcels belonging to customer";
    public static final String PARCELS_FROM_CATEGORY = "Parcels from category";
    public static final String PARCELS_IN_WAREHOUSE = "Parcels in warehouse";
    public static final String PARCELS = "Parcels";
    private final SessionFactory sessionFactory;

    @Autowired
    public ParcelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Parcels> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcels> query = session.createQuery("from Parcels", Parcels.class);

            isQueryEmpty(query, NO_PARCELS_FOUND);
            return query.list();
        }
    }

    @Override
    public void create(Parcels parcels) {
        try (Session session = sessionFactory.openSession()) {
            session.save(parcels);
            createUserParcel(parcels);
        }
    }

    public void createUserParcel(Parcels parcels) {
        try (Session session = sessionFactory.openSession()) {
            int parcelID = parcels.getId();
            int userID = parcels.getCustomer().getId();
            UserParcels userParcels = new UserParcels();
            userParcels.setUserId(userID);
            userParcels.setParcel_id(parcelID);
            session.save(userParcels);
        }
    }

    @Override
    public Parcels getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Parcels parcels = session.get(Parcels.class, id);
            if (parcels == null) {
                throw new EntityNotFoundException("Parcel", id);
            }
            return parcels;
        }
    }

    @Override
    public void update(Parcels parcels) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(parcels);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Parcels parcel) {
        try (Session session = sessionFactory.openSession()) {
            deleteUserParcel(parcel);

            session.beginTransaction();
            session.delete(parcel);
            session.getTransaction().commit();
        }
    }

    public void deleteUserParcel(Parcels parcels) {
        try (Session session = sessionFactory.openSession()) {
            int parcelID = parcels.getId();
            int userID = parcels.getCustomer().getId();

            UserParcels userParcels = new UserParcels();
            userParcels.setUserId(userID);
            userParcels.setParcel_id(parcelID);

            session.beginTransaction();
            session.delete(userParcels);
            session.getTransaction().commit();
        }
    }

    @Override
    //todo to rework the query, to compare min max in the query not after that
    public List<Parcels> filterByWeight(Integer min, Integer max) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcels> query = session.createQuery("from Parcels ", Parcels.class);

            List<Parcels> list = new ArrayList<>();

            double minD = min.doubleValue();
            double maxD = max.doubleValue();

            for (Parcels parcels : query.list()) {
                if (parcels.getWeight() >= minD && parcels.getWeight() <= maxD) {
                    list.add(parcels);
                }
            }

            if (list.isEmpty()) {
                throw new EntityNotFoundException(NO_PARCELS_FOUND);
            }
            return list;
        }
    }

    @Override
    public List<Parcels> filterByCustomer(Optional<Integer> customerId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcels> query = session.createQuery(
                    "from Parcels where customer.id = :customerId", Parcels.class);
            query.setParameter("customerId", customerId.orElse(0));

            return query.list();
        }
    }

    @Override
    public List<Parcels> filterByWarehouse(Optional<Integer> warehouseId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcels> query = session.createQuery(
                    "from Parcels where warehouse.id = :warehouseId", Parcels.class);
            query.setParameter("warehouseId", warehouseId.orElse(0));

            return query.list();
        }
    }

    @Override
    public List<Parcels> filterByCategory(Optional<Integer> categoryId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcels> query = session.createQuery(
                    "from Parcels where category.id = :categoryId", Parcels.class);
            query.setParameter("categoryId", categoryId.orElse(0));

            return query.list();
        }
    }

    @Override
    public List<Parcels> getCustomerOwnParcels(int customerId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcels> query = session.createQuery(
                    "from Parcels where customer.id = :customerId", Parcels.class);
            query.setParameter("customerId", customerId);

            isQueryEmptyId(query, PARCELS_BELONGING_TO_CUSTOMER, customerId);
            return query.list();
        }
    }

    @Override
    public List<Parcels> sortByWeight() {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcels> query = session.createQuery(
                    "from Parcels order by weight asc", Parcels.class);

            isQueryEmpty(query, NO_PARCELS_FOUND);
            return query.list();
        }
    }

    @Override
    public List<Parcels> sortByDate() {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcels> query = session.createQuery(
                    "from Parcels order by shipment.arrival_date desc", Parcels.class);

            isQueryEmpty(query, NO_PARCELS_FOUND);
            return query.list();
        }
    }

    @Override
    //todo - what if a parcel has no shipment - it wont display
    public List<Parcels> sortByWeightAndDate() {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcels> query = session.createQuery(
                    "from Parcels order by weight asc, shipment.arrival_date desc", Parcels.class);

            isQueryEmpty(query, NO_PARCELS_FOUND);
            return query.list();
        }
    }

    private void isQueryEmpty(Query<Parcels> query, String message){
        if (query.list().isEmpty()) {
            throw new EntityNotFoundException(message);
        }
    }


    private void isQueryEmptyId(Query<Parcels> query, String message, int id){
        if (query.list().isEmpty()) {
            throw new EntityNotFoundException(message, id);
        }
    }
}
