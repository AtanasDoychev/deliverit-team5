package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.OrderOfOperationsException;
import com.telerikaacademy.demo.exceptions.UnauthorizedOperationException;
import com.telerikaacademy.demo.models.Parcels;
import com.telerikaacademy.demo.models.Shipment;
import com.telerikaacademy.demo.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Repository
public class ShipmentRepositoryImpl implements ShipmentRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ShipmentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Shipment> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment", Shipment.class);
            return query.list();
        }
    }

    @Override
    public Shipment getCustomerShipments(int parcelID) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcels> query = session.createQuery("from Parcels where id = :parcelID", Parcels.class);
            query.setParameter("parcelID", parcelID);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("There is no parcel with id: %d", parcelID);
            }
            Shipment shipment = query.list().get(0).getShipment();
            int shipmentID = shipment.getId();
            Query<Shipment> query1 = session.createQuery("from Shipment where id = :shipmentID");
            query1.setParameter("shipmentID", shipmentID);
            return query1.list().get(0);
        }
    }

    @Override
    public void create(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.save(shipment);
        }
    }

    @Override
    public void update(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            int shipmentId = shipment.getId();
            Query<Parcels> query = session.createQuery("from Parcels where shipment.id = :shipmentId");
            query.setParameter("shipmentId", shipmentId);
            if (query.list().isEmpty()) {
                throw new OrderOfOperationsException
                        ("Shipment needs to have parcels in it before departure/arrival dates can be set. " +
                                "Please add parcels to the shipment first.");
            } else {
                session.clear();
                session.beginTransaction();
                session.update(shipment);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public Shipment getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Shipment shipment = session.get(Shipment.class, id);
            if (shipment == null) {
                throw new EntityNotFoundException("Shipment", id);
            }
            return shipment;
        }
    }

    @Override
    public void delete(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            int shipmentId = shipment.getId();
            Query<Parcels> query = session.createQuery("from Parcels where shipment.id = :shipmentId");
            query.setParameter("shipmentId", shipmentId);
            if (!query.list().isEmpty()) {
                throw new OrderOfOperationsException
                        ("Shipment cannot be deleted if there are parcels linked to it.");
            } else
                session.beginTransaction();
            session.delete(shipment);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Shipment> getAllWithStatus(String status) {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment", Shipment.class);
            List<Shipment> list = new ArrayList<>();
            for (Shipment shipment : query.list()) {
                if (shipment.getStatusAsString().equalsIgnoreCase(status)) {
                    list.add(shipment);
                }
            }
            if (list.isEmpty()){
                throw new EntityNotFoundException(String.format("There are no shipments with status %s",status));
            }
            return list;
        }
    }

    @Override
    public List<Shipment> getAllFilteredByWarehouse(int warehouseID) {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery
                    ("from Shipment where place_of_departure.primary_key = :warehouseID", Shipment.class);
            query.setParameter("warehouseID", warehouseID);
            return query.list();
        }
    }

    @Override
    public Shipment getNextArrivingShipment() {
        try (Session session = sessionFactory.openSession()) {
            List<Shipment> ontheway = getAllWithStatus("ontheway");
            List<Shipment> preparing = getAllWithStatus("preparing");
            List<Shipment> combined = new ArrayList<>();
            combined.addAll(ontheway);
            combined.addAll(preparing);
            return combined.stream().min(Comparator.comparing(Shipment::getArrival_date)).get();
        }
    }

    @Override
    public List<Shipment> getShipmentByID(Integer shipmentID) {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment where id = :shipmentID");
            query.setParameter("shipmentID", shipmentID);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("There is no shipment with id: %d", shipmentID);
            } else return query.list();
        }
    }

    @Override
    public boolean isUserOwnerOfParcel(User user, int parcelId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcels> query = session.createQuery("from Parcels where id = :parcelID", Parcels.class);
            query.setParameter("parcelID", parcelId);
            String emailOfUserThatTriesToEdit = user.getEmail();
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("There is no parcel with id: %d", parcelId);
            }
            if (emailOfUserThatTriesToEdit.equals(query.list().get(0).getCustomer().getEmail())) {
                return true;
            } else {
                throw new UnauthorizedOperationException("You are not the owner of this parcel.");
            }
        }
    }
}
