package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.models.City;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CityRepositoryImpl implements CityRepository{

    private final SessionFactory sessionFactory;

    @Autowired
    public CityRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<City> getAll() {
        try(Session session = sessionFactory.openSession()){
            Query<City> query = session.createQuery
                    ("from City", City.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Cities not found");
            }
            return query.list();
        }
    }

    @Override
    public City getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            City city = session.get(City.class, id);
            if (city == null) {
                throw new EntityNotFoundException("City", id);
            }
            return city;
        }
    }
}
