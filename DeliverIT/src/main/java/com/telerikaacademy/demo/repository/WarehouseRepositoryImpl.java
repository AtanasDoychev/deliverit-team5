package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.models.Address;
import com.telerikaacademy.demo.models.Warehouse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WarehouseRepositoryImpl implements WarehouseRepository{

    private final SessionFactory sessionFactory;

    @Autowired
    public WarehouseRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Warehouse> getAll() {
        try (Session session = sessionFactory.openSession()){
            Query<Warehouse> query = session.createQuery("from Warehouse", Warehouse.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Warehouses not found");
            }
            return query.list();
        }
    }

    @Override
    public void create(Warehouse warehouse) {
        try (Session session = sessionFactory.openSession()) {
            session.save(warehouse);
        }
    }

    @Override
    public void update(Warehouse warehouse) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(warehouse);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Warehouse warehouseToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(warehouseToDelete);
            session.getTransaction().commit();
        }
    }


    @Override
    public Warehouse getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Warehouse warehouse = session.get(Warehouse.class, id);
            if (warehouse == null) {
                throw new EntityNotFoundException("Warehouse", id);
            }
            return warehouse;
        }
    }

    @Override
    public Warehouse getByAddress(Address address){
        try(Session session = sessionFactory.openSession()){
            Query<Warehouse> query = session.createQuery(
                    "from Warehouse where address = :address", Warehouse.class);
            query.setParameter("address", address);

            List<Warehouse> result = query.list();
            if (result.size() == 0){
                throw new EntityNotFoundException("Warehouse", "address_id",
                        String.valueOf(address.getId()));
            }
            return result.get(0);
        }
    }
}
