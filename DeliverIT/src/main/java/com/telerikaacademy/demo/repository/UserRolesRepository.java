package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.models.Role;

public interface UserRolesRepository {

    Role checkIfEmployeeOrCustomer(int id);
}
