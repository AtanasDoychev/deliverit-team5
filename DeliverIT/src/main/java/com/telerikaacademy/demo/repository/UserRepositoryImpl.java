package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.exceptions.DuplicateEntityException;
import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.exceptions.OrderOfOperationsException;
import com.telerikaacademy.demo.models.Parcels;
import com.telerikaacademy.demo.models.Role;
import com.telerikaacademy.demo.models.User;
import com.telerikaacademy.demo.models.UserRolesHelper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository{

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void createUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery("from Role where role_id = 1", Role.class);
            UserRolesHelper userRoles = new UserRolesHelper();
            session.save(user);
            userRoles.setUserId(user);
            userRoles.setRoleId(query.list().get(0));
            session.save(userRoles);
        }
    }

    @Override
    public void updateUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            User userToDelete = getUserByID(id);
            Query<Parcels> query = session.createQuery("from Parcels where customer.id = :id");
            query.setParameter("id", id);
            if (!query.list().isEmpty()) {
                throw new OrderOfOperationsException
                        ("User cannot be deleted if there are parcels linked to it.");
            } else
                session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> getUserByPartialEmail(Optional<String> email) {
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery("from User where email LIKE :email");
            email.ifPresent(string -> query.setParameter("email","%"+string+"%"));
            if (query.list().isEmpty()){
                throw new EntityNotFoundException("User", "email", email.get());
            } else return query.list();
        }
    }

    @Override
    public List<User> getUserByFirstNameAndLastName(Optional<String> firstname, Optional<String> lastname) {
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery
                    ("from User where firstName = :firstname and lastName = :lastname");
            firstname.ifPresent(string -> query.setParameter("firstname", string));
            lastname.ifPresent(string -> query.setParameter("lastname", string));
            if (query.list().isEmpty()){
                throw new EntityNotFoundException("User with firstname %s and lastname %s", firstname.get(), lastname.get());
            } else return query.list();
        }
    }

    //todo this may brake, if we fill up employees addresses, which might be needed because error 500 in html version
    @Override
    public String getAllCustomersCount() {
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery("from User where address != null", User.class);
            long count = query.stream().count();
            if (count == 0){
                throw new EntityNotFoundException("There are no customers registered yet.");
            }
            return String.format("Number of registered customers is: %d",count);
        }
    }

    @Override
    public List<User> getAll() {
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery("from User", User.class);
            if (query.list().isEmpty()){
                throw new EntityNotFoundException("Empty");
            }
            return query.list();
        }
    }

    @Override
    public List<UserRolesHelper> getAllEmployees() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserRolesHelper> query = session.createQuery(
                    "from UserRolesHelper where roleId = 2", UserRolesHelper.class);
            return query.list();
        }
    }

    @Override
    public List<User> getAllEmployees2() {
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery(
                    "from User where address = null", User.class);
            return query.list();
        }
    }

    @Override
    public List<User> getAllCustomers() {
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery(
                    "from User where address != null", User.class);
            return query.list();
        }
    }

    @Override
    public User getUserByEmail(String email) {
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery(
                    "from User where email = :email", User.class);
            query.setParameter("email", email);
            List<User> list = query.list();
            return list.get(0);
        } catch (Exception e){
            throw new EntityNotFoundException("User with such email does not exist.");
        }
    }

    @Override
    public User getUserByID(int id) {
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery(
                    "from User where id = :id", User.class);
            query.setParameter("id", id);
            List<User> list = query.list();
            return list.get(0);
        } catch (Exception e){
            throw new EntityNotFoundException("User with such ID does not exist.");
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where username = :username", User.class);
            query.setParameter("username", username);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }

            return users.get(0);
        }
    }
}
