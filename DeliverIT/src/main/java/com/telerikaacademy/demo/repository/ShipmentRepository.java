package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.models.Shipment;
import com.telerikaacademy.demo.models.User;

import java.util.List;

public interface ShipmentRepository {

    List<Shipment> getAll();

    Shipment getCustomerShipments(int id);

    void create(Shipment shipment);

    void update(Shipment shipment);

    Shipment getById(int id);

    void delete(Shipment shipment);

    List<Shipment> getAllWithStatus(String status);

    List<Shipment> getAllFilteredByWarehouse(int warehouseID);

    Shipment getNextArrivingShipment();

    boolean isUserOwnerOfParcel(User user, int parcelId);

    List<Shipment> getShipmentByID(Integer shipmentID);
}
