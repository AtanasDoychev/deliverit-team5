package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.models.Address;
import com.telerikaacademy.demo.models.Warehouse;

import java.util.List;

public interface WarehouseRepository {

    List<Warehouse> getAll();

    void create(Warehouse warehouse);

    void update(Warehouse warehouse);

    void delete(int id);

    Warehouse getById(int id);

    Warehouse getByAddress(Address address);
}
