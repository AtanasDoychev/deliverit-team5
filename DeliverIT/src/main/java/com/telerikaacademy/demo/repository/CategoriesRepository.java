package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.models.Categories;

import java.util.List;

public interface CategoriesRepository {

    Categories getById(int id);

    List<Categories> getAll();
}
