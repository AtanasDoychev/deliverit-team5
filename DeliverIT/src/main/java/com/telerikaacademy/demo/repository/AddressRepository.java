package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.models.Address;
import com.telerikaacademy.demo.models.City;

import java.util.List;

public interface AddressRepository {

    List<Address> getAll();

    Address getById(int id);
}
