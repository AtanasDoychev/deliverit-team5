package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.models.Role;
import com.telerikaacademy.demo.models.UserRolesHelper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRolesRepositoryImpl implements UserRolesRepository{

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRolesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Role checkIfEmployeeOrCustomer(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserRolesHelper> query =
                    session.createQuery
                            ("from UserRolesHelper where userId = :id",
                                    UserRolesHelper.class);
            query.setParameter("id", id);
            List<UserRolesHelper> list = query.list();
            return list.get(0).getRoleId();
        }
    }
}
