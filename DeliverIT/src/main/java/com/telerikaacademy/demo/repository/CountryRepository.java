package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.models.Country;

import java.util.List;

public interface CountryRepository {

    List<Country> getAll();
}
