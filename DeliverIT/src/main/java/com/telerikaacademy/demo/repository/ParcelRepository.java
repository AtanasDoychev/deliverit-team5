package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.models.Parcels;
import com.telerikaacademy.demo.models.User;

import java.util.List;
import java.util.Optional;

public interface ParcelRepository {

    List<Parcels> getAll();

    void create(Parcels parcels);

    Parcels getById(int id);

    void update(Parcels parcels);

    void delete(Parcels parcels);

    List<Parcels> filterByWeight(Integer min, Integer max);

    List<Parcels> filterByCustomer(Optional<Integer> customerId);

    List<Parcels> filterByWarehouse(Optional<Integer> warehouseId);

    List<Parcels> filterByCategory(Optional<Integer> categoryId);

    List<Parcels> getCustomerOwnParcels(int customerId);

    List<Parcels> sortByWeight();

    List<Parcels> sortByDate();

    List<Parcels> sortByWeightAndDate();

    //List<Parcels> filterParcels(Optional<Integer> customerId, Optional<Integer> warehouseId, Optional<Integer> categoryId);
}
