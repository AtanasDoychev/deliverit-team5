package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.models.Categories;
import com.telerikaacademy.demo.models.City;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoriesRepositoryImpl implements CategoriesRepository{

    private final SessionFactory sessionFactory;

    @Autowired
    public CategoriesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Categories getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Categories category = session.get(Categories.class, id);
            if (category == null) {
                throw new EntityNotFoundException("Category", id);
            }
            return category;
        }
    }

    @Override
    public List<Categories> getAll() {
        try(Session session = sessionFactory.openSession()){
            Query<Categories> query = session.createQuery
                    ("from Categories ", Categories.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Categories not found");
            }
            return query.list();
        }
    }
}
