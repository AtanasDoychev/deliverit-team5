package com.telerikaacademy.demo.repository;

import com.telerikaacademy.demo.exceptions.EntityNotFoundException;
import com.telerikaacademy.demo.models.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository{

    private final SessionFactory sessionFactory;

    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Country> getAll() {
        try(Session session = sessionFactory.openSession()){
            Query<Country> query = session.createQuery
                    ("from Country", Country.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Countries not found");
            }
            return query.list();
        }
    }
}
