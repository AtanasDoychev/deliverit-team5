package com.telerikaacademy.demo.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exists.", type, attribute, value));
   }
    public DuplicateEntityException(String email) {
        super(String.format("User with email [ %s ] already exists.", email));
    }
}
