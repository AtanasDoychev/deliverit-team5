package com.telerikaacademy.demo.exceptions;

public class OrderOfOperationsException extends RuntimeException{
    public OrderOfOperationsException(String message) {
        super(message);
    }
}
