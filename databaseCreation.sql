create table categories
(
    primary_key int auto_increment
        primary key,
    category    varchar(30) not null,
    constraint categories_category_uindex
        unique (category)
);

create table countries
(
    primary_key int auto_increment
        primary key,
    name        varchar(30) not null,
    constraint countries_name_uindex
        unique (name)
);

create table cities
(
    primary_key int auto_increment
        primary key,
    name        varchar(30) not null,
    country_id  int         not null,
    constraint cities_name_uindex
        unique (name),
    constraint cities_countries_primary_key_fk
        foreign key (country_id) references countries (primary_key)
);

create table addresses
(
    primary_key int auto_increment
        primary key,
    address     varchar(30) not null,
    city_id     int         not null,
    constraint addresses_address_uindex
        unique (address),
    constraint addresses_cities_primary_key_fk
        foreign key (city_id) references cities (primary_key)
);

create table customers
(
    primary_key int auto_increment
        primary key,
    first_name  varchar(15) not null,
    last_name   varchar(15) not null,
    email       varchar(50) null,
    address_id  int         not null,
    constraint customers_email_uindex
        unique (email),
    constraint customers_addresses_primary_key_fk
        foreign key (address_id) references addresses (primary_key)
);

create table employees
(
    primary_key int auto_increment
        primary key,
    first_name  varchar(15) not null,
    last_name   varchar(15) not null,
    email       varchar(30) not null,
    constraint employees_email_uindex
        unique (email)
);

create table statuses
(
    primary_key int auto_increment
        primary key,
    status      varchar(15) not null,
    constraint statuses_status_uindex
        unique (status)
);

create table warehouses
(
    primary_key int auto_increment
        primary key,
    address_id  int not null,
    constraint warehouses_addresses_primary_key_fk
        foreign key (address_id) references addresses (primary_key)
);

create table shipments
(
    primary_key        int auto_increment
        primary key,
    departure_date     date not null,
    arrival_date       date not null,
    place_of_departure int  not null,
    employee_id        int  not null,
    constraint shipments_employees_primary_key_fk
        foreign key (employee_id) references employees (primary_key),
    constraint shipments_warehouses_primary_key_fk_2
        foreign key (place_of_departure) references warehouses (primary_key)
);

create table parcels
(
    primary_key  int auto_increment
        primary key,
    warehouse_id int not null,
    weight       int null,
    category_id  int not null,
    customer_id  int not null,
    shipment_id  int null,
    employee_id  int not null,
    constraint parcels_categories_primary_key_fk
        foreign key (category_id) references categories (primary_key),
    constraint parcels_customers_primary_key_fk
        foreign key (customer_id) references customers (primary_key),
    constraint parcels_employees_primary_key_fk
        foreign key (employee_id) references employees (primary_key),
    constraint parcels_shipments_primary_key_fk
        foreign key (shipment_id) references shipments (primary_key),
    constraint parcels_warehouses_primary_key_fk
        foreign key (warehouse_id) references warehouses (primary_key)
);

