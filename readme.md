DeliverIT is a shipping and warehouse management application. 

Links:
- Trello: https://trello.com/b/rUnNLryo/deliveryit
- Swagger, API-documentation: https://app.swaggerhub.com/apis/Deliverit7/api-documentation/1.0

In order to run the application you need to follow these steps:
1. Clone the repository.
2. Open with IntelliJ
3. Run build.gradle(if it doesn't run automatically)
4. Execute 2 SQL scripts. First DATABASE Creation, then DATALOAD script. (DATA_BASE+DATA_LOAD_07.03.21 Script is a merge
   script from the 2 scripts mentioned above, it's also executable and will do the same as the other two.)
5. Run the application from the DemoApplication class.

Images of the database relations are in DataRelationsImages folder.