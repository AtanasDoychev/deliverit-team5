
-- from here till next comment deletes info from tables and resets auto increment
DELETE FROM parcels;
DELETE FROM shipments;
DELETE FROM statuses;
DELETE FROM categories;
DELETE FROM employees;
DELETE FROM customers;
DELETE FROM warehouses;
DELETE FROM cities;
DELETE FROM countries;

alter table countries AUTO_INCREMENT = 0;
alter table cities AUTO_INCREMENT = 0;
alter table warehouses AUTO_INCREMENT = 0;
alter table customers AUTO_INCREMENT = 0;
alter table employees AUTO_INCREMENT = 0;
alter table statuses AUTO_INCREMENT = 0;
alter table categories AUTO_INCREMENT = 0;
alter table shipments AUTO_INCREMENT = 0;
alter table parcels AUTO_INCREMENT = 0;
-- till here.



-- fill information in tables
INSERT INTO `countries` (`name`) VALUES
('Bulgaria'),
('Germany'),
('USA'),
('Australia'),
('Russia'),
('China');


INSERT INTO `cities` (`name`, country_id)VALUES
('Sofia', 1),
('Varna', 1),
('Burgas', 1),
('Pleven', 1),
('Plovdiv', 1),
('Veliko Tyrnovo', 1),
('Vidin', 1),
('Stara Zagora', 1);

INSERT INTO `warehouses` (`address`,`city_id`) VALUES
('Voivoda 23', 3),
('Dondukov 49', 1),
('Morska 55',2);


INSERT INTO customers(first_name, last_name, email, address, city_id)  values
('Victor', 'Valtchev', 'vic.valtchev@gmail.com', 'Treti Mart 16', 2);

INSERT INTO employees(first_name, last_name, email, warehouse_id) values
('Bai', 'Ivan', 'baiivanmenigdjyra@abv.bg', 1);

INSERT INTO statuses(status) VALUES
('Preparing'),
('On the way'),
('Completed');

INSERT INTO categories(category) VALUES
('Electornics'),
('Clothing'),
('Medical');


INSERT INTO shipments(departure_date, arrival_date, status_id, place_of_departure, employee_id) VALUES
('2021-02-28','2021-03-10', 1, 2, 1);
-- till here.

INSERT INTO parcels(warehouse_id, weight, category_id, customer_id, shipment_id, employee_id) VALUES
(2, 700, 1, 1, null, 1),
(2, 500, 1, 1, 1, 1);



-- commands for joins merges groups, etc.
select *
from cities;


select *
from countries;

select *
from warehouses;

select a.primary_key, a.name, c.name
from cities as a
         join countries c on c.primary_key = a.country_id;

select w.primary_key, w.address, c.name
from warehouses as w
         join cities c on c.primary_key = w.city_id;


-- Show all shipments.
select w.address as 'Warehouse Adress',
       c3.name as 'Warehouse City',
       c4.name as 'Warehouse Country',
       p.weight as ProductWeight,
       c.category,
       c2.first_name as 'Customer First name', c2.last_name as 'Customer Last name',
       s2.status as 'Shipment status',
       c5.name as 'Delivery City',
       c6.name as 'Delivery Country',
       e.first_name as 'Employee First name', e.last_name as 'Employee Last Name'
from parcels as p
         join warehouses w on w.primary_key = p.warehouse_id
         join cities c3 on w.city_id = c3.primary_key
         join countries c4 on c3.country_id = c4.primary_key
         join categories c on c.primary_key = p.category_id
         join customers c2 on c2.primary_key = p.customer_id
         join shipments s on s.primary_key = p.shipment_id
         join employees e on e.primary_key = s.employee_id
         join cities c5 on c2.city_id = c5.primary_key
         join countries c6 on c5.country_id = c6.primary_key
         join statuses s2 on s2.primary_key = s.status_id;
-- till here






